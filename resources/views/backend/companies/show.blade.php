@extends('layouts.main')

@section('content')
  <section class="section">
    <div class="section-header">
      <h1>Mitra Perusahaan Otobus</h1>
    </div>

    <div class="d-sm-flex align-items-center justify-content-start mb-4">
      
    </div>

    <div class="row">
      <div class="col-lg-6">
        <div class="card card-danger">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">Detail Mitra</h6>
          </div>
          <div class="card-body">
            <div>
              Nama<br>
              <strong>{{ $company->name }}</strong>
            </div>
            <hr>
            <div>
              Deskripsi <br>
              <strong>{{ $company->description }}</strong>
            </div>
            <hr>
            <div>
              Lokasi <br>
              <strong>{{ $company->location }}</strong>
            </div>
            <hr>
            <div>
              Status Verifikasi <br>
              <div class="row">
                <div class="col-md-6">
                  @if ($company->is_verification == 1)
                    <span class="badge badge-success">Terverifikasi</span>
                  @else
                    <span class="badge badge-warning">Belum diverifikasi</span>
                  @endif
                </div>
                <div class="col-md-6 text-right">
                  @if ($company->is_verification == 0)  
                    <button class="btn btn-success btn-sm" id="btn-verification"><i class="fa fa-check"></i> Klik untuk verifikasi</button>
                  @endif
                </div>
              </div>
            </div>
            <hr>
            <div>
              Saldo Tersimpan <br>
              <strong>Rp {{ rupiah($company->balances) }}</strong>
            </div>
            <hr>
            <div>
              Logo <br><br>
              @if ($company->image == null)
                <img src="{{ asset('assets/stisla/img/example-image.jpg') }}" class="rounded" width="168" alt="image-logo">  
              @else
                <img alt="image" src="{{ asset('uploads/images/company/'.$company->image) }}" class="rounded" width="168" alt="image-logo">
              @endif
            </div>

          </div>
        </div>
      </div>

      <div class="col-lg-6">
        <div class="card card-danger">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">Informasi Pemilik</h6>
          </div>
          <div class="card-body">
            <div>
              Nama<br>
              <strong>{{ $company->user->name }}</strong>
            </div>
            <hr>
            <div>
              Nomor KTP <br>
              <strong>{{ $company->user->identity_card }}</strong>
            </div>
            <hr>
            <div>
              Email <br>
              <strong>{{ $company->user->email }}</strong>
            </div>
            <hr>
            <div>
              Nomor HP <br>
              <strong>{{ $company->user->phone }}</strong>
            </div>
            <hr>
            <div>
              Foto Pemilik <br><br>
              @if (is_null($company->user->avatar))
                <img src="{{ asset('assets/stisla/img/avatar/avatar.jpg') }}" class="rounded-circle" id="avatar-prev" width="168" height="168" alt="avatar">
              @else
                <img alt="image" src="{{asset('uploads/images/avatars/'.$company->user->avatar)}}" class="rounded-circle" id="avatar-prev" width="168" height="168" alt="avatar">
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <form action="{{ route('company.verification') }}" method="post" id="form-verif">
    @csrf
    <input type="hidden" name="tokenizer" value="{{ $id_crypt }}">
  </form>
@endsection
@section('script')
<script>
  $('#btn-verification').on('click', function() {
    swal({
      title: 'Apakah anda yakin?',
      text: 'Akan melakukan verifikasi untuk PO ini?',
      icon: 'info',
      buttons: true,
    })
    .then((confirm) => {
      if (confirm) {
        $('#form-verif').submit();
      } 
    });
  });
</script>
@endsection
