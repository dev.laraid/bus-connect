@extends('layouts.main')

@section('css')
  <style>
    .param {
        margin-bottom: 7px;
        line-height: 1.4;
    }
    .param-inline dt {
        display: inline-block;
    }
    .param dt {
        margin: 0;
        margin-right: 7px;
        font-weight: 600;
    }
    .param-inline dd {
        vertical-align: baseline;
        display: inline-block;
    }

    .param dd {
        margin: 0;
        vertical-align: baseline;
    } 

    .shopping-cart-wrap .price {
        color: #007bff;
        font-size: 15px;
        font-weight: bold;
        margin-right: 5px;
        display: block;
    }
    var {
        font-style: normal;
    }

    .media img {
        margin-right: 1rem;
    }
    .img-sm {
        width: 90px;
        max-height: 75px;
        object-fit: cover;
    }
  </style>
@endsection

@section('content')
  <section class="section">
    <div class="section-header">
      <h1>Detail Pembayaran</h1>
    </div>

    <div class="d-sm-flex align-items-center justify-content-start mb-4">
    </div>

    <div class="row">
      
      <div class="col-lg-4">
        <div class="card card-danger">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">Informasi Pembayaran</h6>
          </div>
          <div class="card-body">
            <div>
              Invoice<br>
              <strong>{{ $payment->booking->invoice }}</strong>
            </div>
            <hr>
            <div>
              Tranfser Dari <br>
              <strong>{{ $payment->method }}</strong>
            </div>
            <hr>
            <div>
              Atas Nama <br>
              <strong>{{ $payment->account_holder }}</strong>
            </div>
            <hr>
            <div>
              Jumlah Dibayar <br>
              <strong>Rp {{ rupiah($payment->paid) }}</strong>
            </div>
            <hr>
            <div>
              Status Verifikasi <br>
              @if ($payment->is_verification == 1)
                <span class="badge badge-success">Terverifikasi</span>
              @else
                <span class="badge badge-warning">Belum diverifikasi</span>
              @endif
              @if ($payment->is_verification == 0)  
                <button class="btn btn-success btn-sm" id="btn-verification"><i class="fa fa-check"></i> Klik untuk verifikasi</button>
              @endif
            </div>
            <hr>
            <div>
              Bukti Transfer <br><br>
              @if (is_null($payment->image))
                <img src="{{ asset('assets/stisla/img/example-image.jpg') }}" class="rounded" id="avatar-prev" width="168" alt="image">
              @else
                <img alt="image" src="{{asset('uploads/images/payments/'.$payment->image)}}" class="rounded" id="avatar-prev" width="180" alt="image">
              @endif
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-8">
        <div class="card card-danger">
          <div class="card-header">
            Sewa dari : &nbsp;<strong>{{ $booking->company->name }}</strong> &nbsp; - Untuk : &nbsp;<strong>{{ $booking->customer->company->name }}</strong>
          </div>
          <div class="card-body">
            <div class="row">
              <form action="" id="frm-date" class="form-inline col-md-12" method="post">
                <label class="mb-2">Tanggal Sewa : </label>
                <strong class="mb-2 ml-2">{{ $booking->start_date ?? date('Y-m-d') }} sd {{ $booking->end_date ?? date('Y-m-d') }}</strong>
              </form>
            </div>
            <table class="table table-bordered table-hover shopping-cart-wrap">
              <thead class="text-muted">
              <tr>
                <th scope="col">Bus</th>
                <th scope="col" width="150">Jumlah</th>
                <th class="text-right" scope="col" width="180">Total Harga</th>
              </tr>
              </thead>
                <tbody>
                  @php $subtotal = 0; @endphp
                  @foreach ($booking->details as $item)
                    @php
                      $price_total = ((int) $item->price) * $item->quantity;
                      $subtotal += $price_total;
                    @endphp
                    <tr>
                      <td>
                        <figure class="media mt-2">
                          <div class="img-wrap"><img src="{{ asset('uploads/images/vehicles/'. $item->vehicle->image) }}" class="img-thumbnail img-sm"></div>
                          <figcaption class="media-body">
                            <h6 class="title text-truncate">{{ $item->name }}</h6>
                            <dl class="param param-inline small">
                              <dt>Seat : </dt>
                              <dd>{{ $item->seat }}</dd>
                            </dl>
                            <dl class="param param-inline small text-danger">
                              <dt>Harga : </dt>
                              <dd>Rp {{ rupiah((int) $item->price) }}</dd>
                            </dl>
                          </figcaption>
                        </figure> 
                      </td>
                      <td> 
                        <input type="text" value="{{ $item->quantity }}" class="form-control" disabled>
                      </td>
                      <td class="text-right"> 
                        <div class="price-wrap"> 
                          <var class="price">Rp {{ rupiah($price_total) }}</var> 
                        </div>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
                <tfoot>
                  @if ($booking)
                    <tr>
                      <th class="text-right" colspan="2">Subtotal</th>
                      <td class="text-right">
                        <div class="price-wrap"> 
                          <var class="price">Rp {{ rupiah($subtotal) }}</var> 
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <th class="text-right" colspan="2">Lama Sewa</th>
                      <td class="text-right">
                        <div class="price-wrap"> 
                          <var class="price"><span id="days">{{ $diff_days }}</span> Hari</var> 
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <th class="text-right" colspan="2">Total</th>
                      <td class="text-right">
                        <div class="price-wrap"> 
                          <var class="price">Rp {{ rupiah($subtotal * $diff_days) }}</var> 
                        </div>
                      </td>
                    </tr>
                  @endif
                </tfoot>
              </table>
          </div>
        </div>
      </div>
    </div>

  </section>

  <form action="{{ route('payment.update', $payment->id) }}" method="post" id="form-verif">
    @csrf
    @method('PUT')
  </form>
@endsection
@section('script')
<script>
  $('#btn-verification').on('click', function() {
    swal({
      title: 'Apakah anda yakin?',
      text: 'Akan melakukan verifikasi untuk Pembayaran ini?',
      icon: 'info',
      buttons: true,
    })
    .then((confirm) => {
      if (confirm) {
        $('#form-verif').submit();
      } 
    });
  });
</script>
@endsection
