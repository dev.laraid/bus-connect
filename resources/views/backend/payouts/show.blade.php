@extends('layouts.front')

@section('css')
  <link rel="stylesheet" href="{{ asset('assets/stisla/modules/summernote/summernote-bs4.css') }}">
@endsection

@section('content')
  <section class="section">
    <div class="section-header">
      <h1>Lihat Pencairan Saldo</h1>
    </div>

    <form id="frm-payout" method="POST" action="" enctype="multipart/form-data">
      <div class="row">
        <div class="col-lg-8">
          <div class="card card-danger">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold">Informasi Pecairan Saldo</h6>
            </div>
            <div class="card-body">

              <div class="row">
                <div class="form-group col-12 {{ $errors->has('nominal') ? ' has-error' : '' }}">
                  <label for="nominal">Nominal Pencairan</label>
                  <input id="nominal" type="text" class="form-control @if ($errors->has('nominal')) is-invalid @endif" name="nominal" value="Rp {{ rupiah($payout->nominal) }}" readonly>
                  @if ($errors->has('nominal'))
                    <div class="invalid-feedback">
                      {{ $errors->first('nominal') }}
                    </div>
                  @endif
                </div>
              </div>

              <div class="row">
                <div class="form-group col-12 {{ $errors->has('admin_fee') ? ' has-error' : '' }}">
                  <label for="admin_fee">Biaya Admin</label>
                  <input id="admin_fee" type="text" class="form-control @if ($errors->has('admin_fee')) is-invalid @endif" name="admin_fee" value="Rp {{ rupiah($payout->admin_fee) }}" readonly>
                  @if ($errors->has('admin_fee'))
                    <div class="invalid-feedback">
                      {{ $errors->first('admin_fee') }}
                    </div>
                  @endif
                </div>
              </div>

              <div class="row">
                <div class="form-group col-12 {{ $errors->has('payout_total') ? ' has-error' : '' }}">
                  <label for="payout_total">Uang Akan Dicairkan</label>
                  <input id="payout_total" type="text" class="form-control @if ($errors->has('payout_total')) is-invalid @endif" name="payout_total" value="Rp {{ rupiah($payout->payout_total) }}" readonly>
                  @if ($errors->has('payout_total'))
                    <div class="invalid-feedback">
                      {{ $errors->first('payout_total') }}
                    </div>
                  @endif
                </div>
              </div>

              <div class="row">
                <div class="form-group col-12">
                  <label for="payout_total">Nominal Terbilang</label>
                  <h3 class="terbilang">-</h3>
                </div>
              </div>

              <div class="row">
                <div class="form-group col-12">
                  <label for="display_status">Status</label>
                  <input id="display_status" type="text" class="form-control " value="{{ $payout->display_status }}" readonly>
                </div>
              </div>
              
            </div>
          </div>
        </div>

        <div class="col-lg-4">
          <div class="card card-danger">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold">Info Bank Pencairan</h6>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="form-group col-12">
                  <label for="bank_name">Nama Bank</label>
                  <input id="bank_name" type="text" class="form-control " value="{{ $payout->company->bank_name }}" readonly>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-12">
                  <label for="bank_number">Nomor Rekening</label>
                  <input id="bank_number" type="text" class="form-control " value="{{ $payout->company->bank_number }}" readonly>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-12">
                  <label for="bank_account">Bank Atas Nama</label>
                  <input id="bank_account" type="text" class="form-control " value="{{ $payout->company->bank_account }}" readonly>
                </div>
              </div>
            </div>
          </div>

          @if ($payout->status == 100)    
            <div class="card card-danger">
              <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold">Form Aksi</h6>
              </div>
              <div class="card-body">
                <form action="{{ route('payouts.update', $payout->id) }}" method="POST">
                  @csrf
                  @method('PATCH')

                  <div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
                    <label for="status" class="d-block">Setujui Pencairan Saldo</label>
                    <select name="status" class="form-control select2" id="status" onchange="checkAction()">
                      <option value="200" {{ old('status') == 200 ? 'selected' : '' }}>Ya</option>
                      <option value="10" {{ old('status') == 10 ? 'selected' : '' }}>Tidak</option>
                    </select>
                    @if ($errors->has('status'))
                      <div class="invalid-feedback">
                        <strong>{{ $errors->first('status') }}</strong>
                      </div>
                    @endif
                  </div>

                  <div class="form-group custom-file mb-3 up-bukti {{ $errors->has('image') ? ' has-error' : '' }}">
                    <input id="image" type="file" class="custom-file-input @if ($errors->has('image')) is-invalid @endif" name="image">
                    <label class="custom-file-label" for="customFile">Upload Bukti Transfer</label>
                    @if ($errors->has('image'))
                      <div class="invalid-feedback">
                        <strong>{{ $errors->first('image') }}</strong>
                      </div>
                    @endif
                  </div>

                  <div class="form-group {{ $errors->has('reject_reason') ? ' has-error' : '' }}" id="reject-section" style="display: none;">
                    <label for="reject_reason" class="d-block">Alasan menolak</label>
                    <textarea name="reject_reason" class="form-control @if ($errors->has('reject_reason')) is-invalid @endif"  cols="30" rows="10">{{ old('reject_reason') }}</textarea>
                    @if ($errors->has('reject_reason'))
                      <div class="invalid-feedback">
                        <strong>{{ $errors->first('reject_reason') }}</strong>
                      </div>
                    @endif
                  </div>

                  <button class="btn btn-danger btn-block">Proses</button>
                </form>
              </div>
            </div>
          @endif


        </div>
      </div>
    </form>
  </section>
@endsection

@section('script')
  <script src="{{ asset('js/terbilang.js') }}"></script>
  <script>
    $(document).ready(function () {
      bsCustomFileInput.init()
      $('.select2').select2();
      showTerbilang();
    });

    function numberFormat(x) {
      return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
    }

    function showTerbilang() {
      let val = {{ $payout->nominal }};
      let result = terbilang(val);
      $('.terbilang').html(result);
    }

    function checkAction(){
      var status = $('#status').val();

      if (status == 10) {
        $('.up-bukti').hide();
        $('#reject-section').show();
      } else {
        $('.up-bukti').show();
        $('#reject-section').hide();
      }

    }
  </script>
@endsection