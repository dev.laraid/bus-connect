@extends('layouts.main')

@section('content')
  <section class="section">
    <div class="section-header">
      <h1>Transaksi Penyewaan</h1>
    </div>

    <div class="d-sm-flex align-items-center justify-content-start mb-4">
      {{-- <a class="btn btn-sm btn-danger mr-auto" href="{{ route('vehicle.create') }}"><i class="fa fa-plus"></i> Tambah Transaksi Masuk</a>  --}}
      <div class="form-inline">
        <label>Filter Status</label>
        <select name="status" class="form-control-sm ml-2">
          <option value="all">Semua</option>
          <option value="100">Menunggu Pembayaran</option>
          <option value="200">Telah Dibayar</option>
          <option value="300">Selesai</option>
          <option value="10">Dibatalkan</option>
        </select>
        <button class="btn btn-sm btn-danger ml-2" id="btn-filter"><i class="fas fa-filter"></i></button>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">

        <!-- Basic Card Example -->
        <div class="card card-danger">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">List Transaksi Penyewaan</h6>
          </div>
          <div class="card-body">

          <div class="table-responsive">
            <table class="table table-striped datatable">
              <thead>                                 
                <tr>
                  <th>#</th>
                  <th>Invoice</th>
                  <th>Customer</th>
                  <th>Tgl. Sewa</th>
                  <th>Tgl. Selesai</th>
                  <th>Jml. Bus</th>
                  <th>Total Harga</th>
                  <th>Status</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>

          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
@section('script')
<script>
  $(document).ready(function() {
      $('.datatable').DataTable({
          processing: true,
          serverSide: true,
          autoWidth: false,
          language: {
              url: '{{ asset('assets/stisla/modules/datatables/lang/Indonesian.json') }}'
          },
          ajax: {
            url: '{{ route('booking.index') }}',
            data: function (d) {
              d.status = $('select[name=status]').val()
            }
          },
          columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'invoice', name: 'invoice'},
            {data: null, name: 'customer.name', render: function ( data, type, row ) {
              return data.customer.name + '<br><small>(' + data.customer.company.name + ')</small>';
            }},
            {data: 'start_date', name: 'start_date'},
            {data: 'end_date', name: 'end_date'},
            {data: 'vehicle_total', name: 'vehicle_total'},
            {data: null, name: 'price_total', render: function ( data, type, row ) {
              return 'Rp ' + numberFormat(parseInt(data.price_total));
            }},
            {data: 'display_status', name: 'status', orderable: false, searchable: false},
            {data: 'action', name: 'action', orderable: false, searchable: false},
          ]
      });

      $('#btn-filter').click(function(){
         $('.datatable').DataTable().draw(true);
      });

      $(document).on('click','.js-submit-confirm', function(e){
          e.preventDefault();
          swal({
            title: 'Apakah anda yakin ingin menghapus?',
            text: 'Data yang sudah dihapus, tidak dapat dikembalikan!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              $(this).closest('form').submit();
            } 
          });
      });
  });

  function numberFormat(x) {
    return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
  }

</script>
@endsection
