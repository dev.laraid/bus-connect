@extends('layouts.front')

@section('css')
  <link rel="stylesheet" href="{{ asset('assets/stisla/modules/summernote/summernote-bs4.css') }}">
@endsection

@section('content')
  <section class="section">
    <div class="section-header">
      <h1>Akun</h1>
    </div>

    <form method="POST" action="{{ route('profile.update.member', $user->id) }}" enctype="multipart/form-data">
      <div class="row">
        <div class="col-lg-8">
          <div class="card card-danger">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold">Informasi Pengguna</h6>
            </div>
            <div class="card-body">
              @csrf
              @method('PUT')

              <div class="row">
                <div class="form-group col-6 {{ $errors->has('name') ? ' has-error' : '' }}">
                  <label for="name">Nama Lengkap</label>
                  <input id="name" type="text" class="form-control @if ($errors->has('name')) is-invalid @endif" name="name" value="{{ $user->name }}">
                  @if ($errors->has('name'))
                    <div class="invalid-feedback">
                      {{ $errors->first('name') }}
                    </div>
                  @endif
                </div>
                
                <div class="form-group col-6 {{ $errors->has('email') ? ' has-error' : '' }}">
                  <label for="email">Email</label>
                  <input id="email" type="email" class="form-control @if ($errors->has('email')) is-invalid @endif" name="email" value="{{ $user->email }}">
                  @if ($errors->has('email'))
                    <div class="invalid-feedback">
                      {{ $errors->first('email') }}
                    </div>
                  @endif
                </div>
              </div>

              <div class="row">
                <div class="form-group col-5 {{ $errors->has('phone') ? ' has-error' : '' }}">
                  <label for="phone">Nomor Telepon</label>
                  <input id="phone" type="text" class="form-control @if ($errors->has('phone')) is-invalid @endif" name="phone" value="{{ $user->phone }}">
                  @if ($errors->has('phone'))
                    <div class="invalid-feedback">
                      {{ $errors->first('phone') }}
                    </div>
                  @endif
                </div>

                <div class="form-group col-7 {{ $errors->has('identity_card') ? ' has-error' : '' }}">
                  <label for="identity_card">Nomor KTP</label>
                  <input id="identity_card" type="text" class="form-control @if ($errors->has('identity_card')) is-invalid @endif" name="identity_card" value="{{ $user->identity_card }}">
                  @if ($errors->has('identity_card'))
                    <div class="invalid-feedback">
                      {{ $errors->first('identity_card') }}
                    </div>
                  @endif
                </div>
              </div>
              
            </div>
          </div>

          <div class="card card-danger">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold">Informasi Perusahaan</h6>
            </div>
            <div class="card-body">

              <div class="row">
                <div class="form-group col-12 {{ $errors->has('company_name') ? ' has-error' : '' }}">
                  <label for="company_name">Nama PO</label>
                  <input id="company_name" type="text" class="form-control @if ($errors->has('company_name')) is-invalid @endif" name="company_name" value="{{ $company->name }}">
                  @if ($errors->has('company_name'))
                    <div class="invalid-feedback">
                      {{ $errors->first('company_name') }}
                    </div>
                  @endif
                </div>
              </div>

              <div class="row">
                <div class="form-group col-12 {{ $errors->has('company_description') ? ' has-error' : '' }}">
                  <label for="company_description">Deskripsi</label>
                  <textarea id="company_description" class="summernote @if ($errors->has('company_description')) is-invalid @endif" name="company_description" >{{ $company->description }}</textarea>
                  @if ($errors->has('company_description'))
                    <div class="invalid-feedback">
                      {{ $errors->first('company_description') }}
                    </div>
                  @endif
                </div>
              </div>

              <div class="row">
                <div class="form-group col-12 {{ $errors->has('company_location') ? ' has-error' : '' }}">
                  <label for="company_location">Lokasi</label>
                  <input id="company_location" type="text" class="form-control @if ($errors->has('company_location')) is-invalid @endif" name="company_location" value="{{ $company->location }}">
                  @if ($errors->has('company_location'))
                    <div class="invalid-feedback">
                      {{ $errors->first('company_location') }}
                    </div>
                  @endif
                </div>
              </div>

              <div class="row">
                <div class="form-group col-12 {{ $errors->has('company_bank_name') ? ' has-error' : '' }}">
                  <label for="company_bank_name">Nama Bank</label>
                  <select class="form-control @if ($errors->has('company_bank_name')) is-invalid @endif select2" name="company_bank_name" id="company_bank_name" data-placeholder="Silahkan pilih bank" required>
                    <option></option>
                    @foreach ($banks as $item)
                      <option value="{{ $item['name'] }}" {{ $item["name"] == $company->bank_name ? 'selected' : '' }}>{{ $item['name'] }}</option>
                    @endforeach
                  </select>
                  @if ($errors->has('company_bank_name'))
                    <div class="invalid-feedback">
                      {{ $errors->first('company_bank_name') }}
                    </div>
                  @endif
                </div>
              </div>

              <div class="row">
                <div class="form-group col-12 {{ $errors->has('company_bank_account') ? ' has-error' : '' }}">
                  <label for="company_bank_account">Bank Atas Nama</label>
                  <input id="company_bank_account" type="text" class="form-control @if ($errors->has('company_bank_account')) is-invalid @endif" name="company_bank_account" value="{{ $company->bank_account }}">
                  @if ($errors->has('company_bank_account'))
                    <div class="invalid-feedback">
                      {{ $errors->first('company_bank_account') }}
                    </div>
                  @endif
                </div>
              </div>

              <div class="row">
                <div class="form-group col-12 {{ $errors->has('company_bank_number') ? ' has-error' : '' }}">
                  <label for="company_bank_number">Bank Nomor Rekening</label>
                  <input id="company_bank_number" type="number" class="form-control @if ($errors->has('company_bank_number')) is-invalid @endif" name="company_bank_number" value="{{ $company->bank_number }}">
                  @if ($errors->has('company_bank_number'))
                    <div class="invalid-feedback">
                      {{ $errors->first('company_bank_number') }}
                    </div>
                  @endif
                </div>
              </div>

              <div class="form-group">
                <button type="submit" class="btn btn-danger btn-block" tabindex="4">
                  Update
                </button>
              </div>
              
            </div>
          </div>
        </div>

        <div class="col-lg-4">
          <div class="card card-danger">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold">Profil Avatar</h6>
            </div>
            <div class="card-body">
              <div class="form-group">
                <div class="text-center">
                  @if (is_null($user->avatar))
                    <img src="{{ asset('assets/stisla/img/avatar/avatar.jpg') }}" class="rounded-circle" id="avatar-prev" width="168" height="168" alt="avatar">
                  @else
                    <img alt="image" src="{{asset('uploads/images/avatars/'.$user->avatar)}}" class="rounded-circle" id="avatar-prev" width="168" height="168" alt="avatar">
                  @endif
                </div>
              </div>
              <div class="form-group custom-file mb-3">
                <input id="avatar" type="file" class="custom-file-input {{ $errors->has('avatar') ? ' has-error' : '' }}" name="avatar">
                <label class="custom-file-label" for="customFile">Pilih Gambar</label>
              </div>
              @if ($errors->has('avatar'))
                <div class="invalid-feedback">
                  <strong>{{ $errors->first('avatar') }}</strong>
                </div>
              @endif
            </div>
          </div>

          <div class="card card-danger">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold">Logo Perusahaan</h6>
            </div>
            <div class="card-body">
              <div class="form-group">
                <div class="text-center">
                  @if (is_null($company->image))
                    <img src="{{ asset('assets/stisla/img/example-image.jpg') }}" class="rounded-circle" id="logo-prev" width="168" height="168" alt="avatar">
                  @else
                    <img alt="image" src="{{asset('uploads/images/company/'.$company->image)}}" class="rounded-circle" id="logo-prev" width="168" height="168" alt="avatar">
                  @endif
                </div>
              </div>
              <div class="form-group custom-file mb-3">
                <input id="logo" type="file" class="custom-file-input {{ $errors->has('image') ? ' has-error' : '' }}" name="image">
                <label class="custom-file-label" for="customFile">Pilih Gambar</label>
              </div>
              @if ($errors->has('image'))
                <div class="invalid-feedback">
                  <strong>{{ $errors->first('image') }}</strong>
                </div>
              @endif
            </div>
          </div>
        </div>

      </div>
    </form>
  </section>
@endsection

@section('script')
  <script src="{{ asset('assets/stisla/modules/summernote/summernote-bs4.js') }}"></script>
  <script type="text/javascript">
    $(".summernote").summernote({
        dialogsInBody: true,
        minHeight: 150,
        toolbar: [
          ['style', ['style']],
          ['font', ['bold', 'underline', 'clear']],
          ['fontname', ['fontname']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['view', ['codeview', 'help']],
        ],
    });

    $(document).ready(function () {
      bsCustomFileInput.init()
      $('.select2').select2();
    })

    function readURL(input, target) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $(target).attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
    }

    $("#avatar").change(function() {
      readURL(this, '#avatar-prev');
    });

    $("#logo").change(function() {
      readURL(this, '#logo-prev');
    });
  </script>
@endsection