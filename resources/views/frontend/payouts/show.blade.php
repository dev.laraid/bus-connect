@extends('layouts.front')

@section('css')
  <link rel="stylesheet" href="{{ asset('assets/stisla/modules/summernote/summernote-bs4.css') }}">
@endsection

@section('content')
  <section class="section">
    <div class="section-header">
      <h1>Lihat Pencairan Saldo</h1>
    </div>

    <form id="frm-payout" method="POST" action="" enctype="multipart/form-data">
      <div class="row">
        <div class="col-lg-8">
          <div class="card card-danger">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold">Lihat Pecairan Saldo</h6>
            </div>
            <div class="card-body">
              {{ csrf_field() }}

              <div class="row">
                <div class="form-group col-12 {{ $errors->has('nominal') ? ' has-error' : '' }}">
                  <label for="nominal">Nominal Pencairan</label>
                  <input id="nominal" type="text" class="form-control @if ($errors->has('nominal')) is-invalid @endif" name="nominal" value="Rp {{ rupiah($payout->nominal) }}" readonly>
                  @if ($errors->has('nominal'))
                    <div class="invalid-feedback">
                      {{ $errors->first('nominal') }}
                    </div>
                  @endif
                </div>
              </div>

              <div class="row">
                <div class="form-group col-12 {{ $errors->has('admin_fee') ? ' has-error' : '' }}">
                  <label for="admin_fee">Biaya Admin</label>
                  <input id="admin_fee" type="text" class="form-control @if ($errors->has('admin_fee')) is-invalid @endif" name="admin_fee" value="Rp {{ rupiah($payout->admin_fee) }}" readonly>
                  @if ($errors->has('admin_fee'))
                    <div class="invalid-feedback">
                      {{ $errors->first('admin_fee') }}
                    </div>
                  @endif
                </div>
              </div>

              <div class="row">
                <div class="form-group col-12 {{ $errors->has('payout_total') ? ' has-error' : '' }}">
                  <label for="payout_total">Uang Akan Dicairkan</label>
                  <input id="payout_total" type="text" class="form-control @if ($errors->has('payout_total')) is-invalid @endif" name="payout_total" value="Rp {{ rupiah($payout->payout_total) }}" readonly>
                  @if ($errors->has('payout_total'))
                    <div class="invalid-feedback">
                      {{ $errors->first('payout_total') }}
                    </div>
                  @endif
                </div>
              </div>

              <div class="row">
                <div class="form-group col-12">
                  <label for="payout_total">Nominal Terbilang</label>
                  <h3 class="terbilang">-</h3>
                </div>
              </div>

              <div class="row">
                <div class="form-group col-12">
                  <label for="display_status">Status</label>
                  <input id="display_status" type="text" class="form-control " value="{{ $payout->display_status }}" readonly>
                </div>
              </div>
              
            </div>
          </div>
        </div>

        <div class="col-lg-4">
          <div class="row">
             <div class="col-lg-12 col-md-6 col-sm-6 col-12">
              <div class="card card-statistic-1">
                <div class="card-icon bg-info">
                  <i class="fas fa-dollar-sign"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>Saldo Tersimpan</h4>
                  </div>
                  <div class="card-body">
                    Rp {{ rupiah($data['balances']) }}
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-12 col-md-6 col-sm-6 col-12">
              <div class="card card-statistic-1">
                <div class="card-icon bg-danger">
                  <i class="fas fa-dollar-sign"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>Saldo Dicairkan</h4>
                  </div>
                  <div class="card-body">
                    Rp {{ rupiah($data['payouts']) }}
                  </div>
                </div>
              </div>
            </div>

            @if ($payout->is_payout == 1 && $payout->status == 200)    
              <div class="col-lg-12 col-md-6 col-sm-6 col-12">
                <div class="card card-danger">
                  <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold">Bukti Transfer Pencairan</h6>
                  </div>
                  <div class="card-body">
                    <div class="form-group">
                      <div class="text-center">
                        @if (is_null($payout->image))
                          <img src="{{ asset('assets/stisla/img/example-image.jpg') }}" class="rounded" id="logo-prev" width="210" alt="bukti">
                        @else
                          <img alt="image" src="{{asset('uploads/images/payouts/'.$payout->image)}}" class="rounded" id="logo-prev" width="210" alt="bukti">
                        @endif
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            @endif

            @if ($payout->status == 10)    
              <div class="col-lg-12 col-md-6 col-sm-6 col-12">
                <div class="alert alert-danger alert-has-icon">
                  <div class="alert-icon"><i class="fa fa-info"></i></div>
                  <div class="alert-body">
                    <div class="alert-title">Pencairan Ditolak</div>
                    <strong>Alasan :</strong> <br> {{ $payout->reject_reason }}
                  </div>
                </div>
              </div>
            @endif

          </div>
        </div>
      </div>
      <input type="hidden" id="limit" value="{{ $data['balances'] }}">
    </form>
  </section>
@endsection

@section('script')
  <script src="{{ asset('js/terbilang.js') }}"></script>
  <script>
    $(document).ready(function () {
      bsCustomFileInput.init()
      $('.select2').select2();
      showTerbilang();
    });

    function numberFormat(x) {
      return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
    }

    function showTerbilang() {
      let val = {{ $payout->nominal }};
      let result = terbilang(val);
      $('.terbilang').html(result);
    }
  </script>
@endsection