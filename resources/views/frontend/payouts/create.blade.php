@extends('layouts.front')

@section('css')
  <link rel="stylesheet" href="{{ asset('assets/stisla/modules/summernote/summernote-bs4.css') }}">
@endsection

@section('content')
  <section class="section">
    <div class="section-header">
      <h1>Request Pencairan Saldo</h1>
    </div>

    <form id="frm-payout" method="POST" action="{{ route('payout.store') }}" enctype="multipart/form-data">
      <div class="row">
        <div class="col-lg-8">
          <div class="card card-danger">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold">Form Pecairan Saldo</h6>
            </div>
            <div class="card-body">
              {{ csrf_field() }}

              <div class="row">
                <div class="form-group col-12 {{ $errors->has('nominal') ? ' has-error' : '' }}">
                  <label for="nominal">Nominal Pencairan</label>
                  <input id="nominal" type="number" class="form-control @if ($errors->has('nominal')) is-invalid @endif" name="nominal" value="{{ old('nominal') }}" onkeyup="calculate()">
                  @if ($errors->has('nominal'))
                    <div class="invalid-feedback">
                      {{ $errors->first('nominal') }}
                    </div>
                  @endif
                </div>
              </div>

              <div class="row">
                <div class="form-group col-12 {{ $errors->has('admin_fee') ? ' has-error' : '' }}">
                  <label for="admin_fee">Biaya Admin</label>
                  <input id="admin_fee" type="text" class="form-control @if ($errors->has('admin_fee')) is-invalid @endif" name="admin_fee" value="{{ old('admin_fee') ?? 0 }}" readonly>
                  @if ($errors->has('admin_fee'))
                    <div class="invalid-feedback">
                      {{ $errors->first('admin_fee') }}
                    </div>
                  @endif
                </div>
              </div>

              <div class="row">
                <div class="form-group col-12 {{ $errors->has('payout_total') ? ' has-error' : '' }}">
                  <label for="payout_total">Uang Akan Dicairkan</label>
                  <input id="payout_total" type="text" class="form-control @if ($errors->has('payout_total')) is-invalid @endif" name="payout_total" value="{{ old('payout_total') ?? 0 }}" readonly>
                  @if ($errors->has('payout_total'))
                    <div class="invalid-feedback">
                      {{ $errors->first('payout_total') }}
                    </div>
                  @endif
                </div>
              </div>

              <div class="row">
                <div class="form-group col-12">
                  <label for="payout_total">Nominal Terbilang</label>
                  <h3 class="terbilang">-</h3>
                </div>
              </div>

              <div class="form-group">
                <button type="button" onclick="validate()" class="btn btn-danger btn-block" tabindex="4">
                  Proses Request
                </button>
              </div>
              
            </div>
          </div>
        </div>

        <div class="col-lg-4">
          <div class="row">
             <div class="col-lg-12 col-md-6 col-sm-6 col-12">
              <div class="card card-statistic-1">
                <div class="card-icon bg-info">
                  <i class="fas fa-dollar-sign"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>Saldo Tersimpan</h4>
                  </div>
                  <div class="card-body">
                    Rp {{ rupiah($data['balances']) }}
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-12 col-md-6 col-sm-6 col-12">
              <div class="card card-statistic-1">
                <div class="card-icon bg-danger">
                  <i class="fas fa-dollar-sign"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>Saldo Dicairkan</h4>
                  </div>
                  <div class="card-body">
                    Rp {{ rupiah($data['payouts']) }}
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-12 col-md-6 col-sm-6 col-12">
              <div class="alert alert-warning">
                <strong><i class="fa fa-info"></i> Info</strong> <br>
                Pencairan saldo akan dikenakan biaya administrasi sebesar 5% dari total pencairan.
              </div>
            </div>
          </div>
        </div>
      </div>
      <input type="hidden" id="limit" value="{{ $data['balances'] }}">
    </form>
  </section>
@endsection

@section('script')
  <script src="{{ asset('js/terbilang.js') }}"></script>
  <script>
    $(document).ready(function () {
      bsCustomFileInput.init()
      $('.select2').select2();
    });

    function numberFormat(x) {
      return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
    }

    function calculate() {
      let val = $('#nominal').val();
      let result = terbilang(val);
      $('.terbilang').html(result);
      var admin_fee = 0.05 * val;
      $('#admin_fee').val(numberFormat(admin_fee));
      $('#payout_total').val(numberFormat(val - admin_fee));
    }

    function validate() {
      var limit = $('#limit').val();
      let val = $('#nominal').val();
      
      if (val > limit) {
        swal(
          'Perhatian!',
          'Nominal pencairan melebihi saldo yang tersimpan, silahkan cek kembali!',
          'error'
        );
        return;
      }
      
      $('#frm-payout').submit();
    }
  </script>
@endsection