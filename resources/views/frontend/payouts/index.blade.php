@extends('layouts.front')

@section('content')
  <section class="section">
    <div class="section-header">
      <h1>Pencairan Saldo</h1>
    </div>

    <div class="d-sm-flex align-items-center justify-content-start mb-4">
      <a class="btn btn-sm btn-danger mr-auto" href="{{ route('payout.create') }}"><i class="fa fa-plus"></i> Request Pencairan Saldo</a> 
      <div class="form-inline">
        <label>Filter Status</label>
        <select name="status" class="form-control-sm ml-2">
          <option value="all">Semua</option>
          <option value="100">Menunggu Pencairan</option>
          <option value="200">Telah Dicairkan</option>
          <option value="300">Selesai</option>
          <option value="10">Dibatalkan</option>
        </select>
        <button class="btn btn-sm btn-danger ml-2" id="btn-filter"><i class="fas fa-filter"></i></button>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">

        <!-- Basic Card Example -->
        <div class="card card-danger">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">List Pencairan Saldo</h6>
          </div>
          <div class="card-body">

          <div class="table-responsive">
            <table class="table table-striped datatable">
              <thead>                                 
                <tr>
                  <th>#</th>
                  <th>Nominal Pencairan</th>
                  <th>Biaya Administrasi</th>
                  <th>Uang Dicairkan</th>
                  <th>Tgl. Pencairan</th>
                  <th>Status</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>

          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
@section('script')
<script>
  $(document).ready(function() {
      $('.datatable').DataTable({
          processing: true,
          serverSide: true,
          autoWidth: false,
          language: {
              url: '{{ asset('assets/stisla/modules/datatables/lang/Indonesian.json') }}'
          },
          ajax: {
            url: '{{ route('payout.index') }}',
            data: function (d) {
              d.status = $('select[name=status]').val()
            }
          },
          columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
            {data: null, name: 'nominal', render: function ( data, type, row ) {
              return 'Rp ' + numberFormat(parseInt(data.nominal));
            }},
            {data: null, name: 'admin_fee', render: function ( data, type, row ) {
              return 'Rp ' + numberFormat(parseInt(data.admin_fee));
            }},
            {data: null, name: 'payout_total', render: function ( data, type, row ) {
              return 'Rp ' + numberFormat(parseInt(data.payout_total));
            }},
            {data: 'display_date', name: 'status', orderable: false, searchable: false},
            {data: 'display_status', name: 'status', orderable: false, searchable: false},
            {data: 'action', name: 'action', orderable: false, searchable: false},
          ]
      });

      $('#btn-filter').click(function(){
         $('.datatable').DataTable().draw(true);
      });

      $(document).on('click','.js-submit-confirm', function(e){
          e.preventDefault();
          swal({
            title: 'Apakah anda yakin ingin menghapus?',
            text: 'Data yang sudah dihapus, tidak dapat dikembalikan!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              $(this).closest('form').submit();
            } 
          });
      });
  });

  function numberFormat(x) {
    return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
  }

</script>
@endsection
