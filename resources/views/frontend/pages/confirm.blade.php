@extends('layouts.front')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/stisla/modules/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

@section('content')
  <section class="section">
    <div class="row">
      <div class="card col-md-6 offset-md-3 mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Informasi Pembayaran</h6>
        </div>
        <div class="card-body">
          <table class="table table-sm">
            <tbody>
              <tr class="bg-info text-white">
                <th width="35%">No. Invoice</th>
                <td>{{ $booking->invoice }}</td>
              </tr>
              <tr>
                <th>Nama Bank</th>
                <td>{{ $bank['name'] }}</td>
              </tr>
              <tr class="bg-info text-white">
                <th>No. Rekening</th>
                <td>{{ $bank['number'] }}</td>
              </tr>
              <tr>
                <th>Atas Nama</th>
                <td>{{ $bank['account_holder'] }}</td>
              </tr>
              <tr class="bg-info text-white">
                <th>Jumlah Transfer</th>
                <td>Rp {{ rupiah($booking->price_total) }}</td>
              </tr>
            </tbody>
          </table>

          @if (!$booking->payment)    
            <div class="alert alert-warning">
              <strong><i class="fa fa-info"></i></strong> Silahkan melakukan transfer sesuai dengan keterangan di atas.
            </div>

            <div class="alert alert-info">
              <strong><i class="far fa-thumbs-up"></i>Tetap di halaman ini.</strong> Untuk melakukan konfirmasi transfer.
            </div>

            <div class="card border-0 bg-color-grey">
              <div class="card-body">
                <i class="icon-check icons text-color-primary text-8"></i>
                <h4 class="card-title mt-2 mb-1 text-4 font-weight-bold">Konfirmasi Transfer</h4>
                <form id="form-payment" action="{{ route('confirm.store', $booking->invoice) }}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div class="form-group {{ $errors->has('confirm_bank') ? ' has-error' : '' }}">
                    <label class="required font-weight-bold text-dark text-2">Tranfer dari bank</label>
                    <select data-msg-required="Isian ini harus diisi." class="form-control select2" name="confirm_bank" id="confirm_bank" data-placeholder="Silahkan pilih bank" required>
                      <option></option>
                      @foreach ($banks as $item)
                        <option value="{{ $item['name'] }}">{{ $item['name'] }}</option>
                      @endforeach
                    </select>
                    @if ($errors->has('confirm_bank'))
                      <div class="invalid-feedback">
                        {{ $errors->first('confirm_bank') }}
                      </div>
                    @endif
                  </div>
                  <div class="form-row">
                    <div class="form-group col-lg-12 {{ $errors->has('confirm_account_holder') ? ' has-error' : '' }}">
                      <label class="required font-weight-bold text-dark text-2">Tranfer Atas Nama</label>
                      <input id="confirm_account_holder" type="text" class="form-control @if ($errors->has('confirm_account_holder')) is-invalid @endif" name="confirm_account_holder" value="{{ old('confirm_account_holder') }}" placeholder="Transfer Atas Nama" data-msg-required="Transfer Atas Nama harus diisi" required>
                      @if ($errors->has('confirm_account_holder'))
                        <div class="invalid-feedback">
                          {{ $errors->first('confirm_account_holder') }}
                        </div>
                      @endif
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-group col {{ $errors->has('payment_image') ? ' has-error' : '' }}">
                      <label class="required font-weight-bold text-dark text-2">Upload Bukti Transfer</label>
                      <div class="custom-file">
                        <input type="file" class="custom-file-input @if ($errors->has('payment_image')) is-invalid @endif" id="" name="payment_image" data-msg-required="Harap upload bukti transfer" required>
                        <label class="custom-file-label">Pilih Foto</label>
                      </div>
                      @if ($errors->has('payment_image'))
                        <div class="invalid-feedback">
                          {{ $errors->first('payment_image') }}
                        </div>
                      @endif
                    </div>
                  </div>	
                  <div class="form-row">
                    <div class="form-group col">
                      <button type="button" onclick="validate(this)" class="btn btn-primary btn-modern text-white">Kirim</button>
                    </div>
                  </div>
                </form>	
              </div>
            </div>
          @else
            <div class="alert alert-success">
              <strong><i class="far fa-thumbs-up"></i>Terima Kasih </strong> telah melakukan konfirmasi transfer, pembayaran anda sedang kami proses.
            </div>

            <div class="alert alert-info">
              <strong><i class="fa fa-info"></i></strong> Untuk informasi selanjutnya akan kami infokan melalui Whatsapp / Email.
            </div>
          @endif

        </div>
      </div>
    </div>
  </section>

@endsection

@section('script')
  <script type="text/javascript" src="{{ asset('assets/stisla/modules/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
  <script>
    function numberFormat(x) {
      return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
    }

    $(document).ready(function () {
      bsCustomFileInput.init()
      $('.select2').select2();
    })

    function validate() {
      var valid = true;
			if (valid) {
					swal({
              title: "Konfirmasi",
              text: "Apakah anda yakin data yang diinput sudah benar?",
              icon: 'warning',
              buttons: {
              cancel: "Tidak",
              confirm: "Ya"
          }})
          .then((value) => {
              if(value == true){
                $('#form-payment').submit();
              }
          });
			}
		}

    @if(Session::has('swal_notification.message'))
      var type = "{{ Session::get('swal_notification.level', 'info') }}";
      switch(type){
        case 'success':
          swal(
            'Sukses!',
            '{{ Session::get('swal_notification.message') }}',
            'success'
          );
          break;

        case 'error':
          swal(
            'Gagal!',
            '{{ Session::get('swal_notification.message') }}',
            'error'
          );
          break;
      }
    @endif
  </script>
@endsection