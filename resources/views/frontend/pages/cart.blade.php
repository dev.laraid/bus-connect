@extends('layouts.front')

@section('css')
    <style>
      .param {
          margin-bottom: 7px;
          line-height: 1.4;
      }
      .param-inline dt {
          display: inline-block;
      }
      .param dt {
          margin: 0;
          margin-right: 7px;
          font-weight: 600;
      }
      .param-inline dd {
          vertical-align: baseline;
          display: inline-block;
      }

      .param dd {
          margin: 0;
          vertical-align: baseline;
      } 

      .shopping-cart-wrap .price {
          color: #007bff;
          font-size: 15px;
          font-weight: bold;
          margin-right: 5px;
          display: block;
      }
      var {
          font-style: normal;
      }

      .media img {
          margin-right: 1rem;
      }
      .img-sm {
          width: 90px;
          max-height: 75px;
          object-fit: cover;
      }
    </style>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/stisla/modules/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

@section('content')
  <section class="section">
    <div class="card mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">List Booking</h6>
      </div>
      <div class="card-body">
        @forelse ($carts as $key => $cart)
          <div class="card card-danger">
            <div class="card-header">
              Sewa ke : &nbsp;<strong>{{ $cart['company_name'] }}</strong>
            </div>
            <div class="card-body">
              <div class="row">
                <form action="{{ route('cart.dateadd') }}" id="frm-date_{{ $key }}" class="form-inline col-md-6" method="post">
                  @csrf
                  <label class="mb-2">Tanggal Sewa</label>
                  <input type="text" class="form-control w-50 mb-2 mx-2" id="date_range_{{ $key }}" name="date_range" value="{{ $cart['date_range'] ?? '' }}">
                  <input type="hidden" id="start_date_{{ $key }}" name="start_date" value="{{ $cart['start_date'] ?? date('Y-m-d') }}"">
                  <input type="hidden" id="end_date_{{ $key }}" name="end_date" value="{{ $cart['end_date'] ?? date('Y-m-d') }}">
                  <input type="hidden" id="diff_days_{{ $key }}" name="diff_days" value="{{ $cart['diff_days'] ?? '1' }}">
                  <input type="hidden" name="company_id" value="{{ $key }}">
                  <button type="submit" class="btn btn-info mb-2">Submit Tanggal</button>
                </form>
              </div>
              <table class="table table-bordered table-hover shopping-cart-wrap">
                <thead class="text-muted">
                <tr>
                  <th scope="col">Bus</th>
                  <th scope="col" width="150">Jumlah</th>
                  <th scope="col" width="180">Total Harga</th>
                  <th scope="col" width="200" class="text-right">Aksi</th>
                </tr>
                </thead>
                  <tbody>
                    @php $subtotal = 0; $diff_days = $cart['diff_days'] ?? '1'; @endphp
                    @foreach ($cart['data'] as $item)
                      @php
                        $price_total = ((int) $item['price']) * $item['quantity'];
                        $subtotal += $price_total;
                      @endphp
                      <tr>
                        <td>
                          <figure class="media mt-2">
                            <div class="img-wrap"><img src="{{ $item['image_url'] }}" class="img-thumbnail img-sm"></div>
                            <figcaption class="media-body">
                              <h6 class="title text-truncate">{{ $item['name'] }}</h6>
                              <dl class="param param-inline small">
                                <dt>Seat : </dt>
                                <dd>{{ $item['seat'] }}</dd>
                              </dl>
                              <dl class="param param-inline small text-danger">
                                <dt>Harga : </dt>
                                <dd>Rp {{ rupiah((int) $item['price']) }}</dd>
                              </dl>
                            </figcaption>
                          </figure> 
                        </td>
                        <td> 
                          <select name="qty" id="qty_{{ $item['id'] }}" class="form-control">
                            @foreach (range(1, $item['available']) as $i)
                              <option value="{{ $i }}" {{ $item['quantity'] == $i ? 'selected' : '' }}>{{ $i }}</option>
                            @endforeach
                          </select>
                        </td>
                        <td> 
                          <div class="price-wrap"> 
                            <var class="price">Rp {{ rupiah($price_total) }}</var> 
                          </div>
                        </td>
                        <td class="text-right"> 
                          <a href="javascript:void(0)" onclick="updateItem({{ $key }}, {{ $item['id'] }})" class="btn btn-outline-warning">Update</a>
                          <a href="javascript:void(0)" onclick="removeItem({{ $key }}, {{ $item['id'] }})" class="btn btn-outline-danger">Hapus</a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                  <tfoot>
                    @if ($cart)
                      <tr>
                        <th class="text-right" colspan="3">Subtotal</th>
                        <td class="text-right" colspan="2">
                          <div class="price-wrap"> 
                            <var class="price">Rp {{ rupiah($subtotal) }}</var> 
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <th class="text-right" colspan="3">Lama Sewa</th>
                        <td class="text-right" colspan="2">
                          <div class="price-wrap"> 
                            <var class="price"><span id="days_{{ $key }}">{{ $diff_days }}</span> Hari</var> 
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <th class="text-right" colspan="3">Total</th>
                        <td class="text-right" colspan="2">
                          <div class="price-wrap"> 
                            <var class="price">Rp {{ rupiah($subtotal * $diff_days) }}</var> 
                          </div>
                        </td>
                      </tr>
                    @endif
                  </tfoot>
                </table>
                @if ($cart)
                  <div class="card-footer text-right">
                    <a href="{{ route('checkout', $key) }}" class="btn btn-danger"><i class="fa fa-chevron-right"></i> Lanjut ke pembayaran</a>
                  </div>
                @endif
            </div>
          </div>
        @empty
          <div class="text-center">
            Belum ada bus yg ditambahkan :(
          </div>
        @endforelse
            
      </div>
    </div>
  </section>

  <form id="frm-remove" action="{{ route('cart.remove') }}" method="post">
    @csrf
    <input type="hidden" id="company_id" name="company_id">
    <input type="hidden" id="vehicle_id" name="vehicle_id">
  </form>

  <form id="frm-update" action="{{ route('cart.update') }}" method="post">
    @csrf
    <input type="hidden" id="comp_id" name="company_id">
    <input type="hidden" id="vc_id" name="vehicle_id">
    <input type="hidden" id="quantity" name="quantity">
  </form>
@endsection

@section('script')
  <script type="text/javascript" src="{{ asset('assets/stisla/modules/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
  <script>
    function numberFormat(x) {
      return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
    }

    function removeItem(c_id, v_id) {
      $('#company_id').val(c_id);
      $('#vehicle_id').val(v_id);

      $('#frm-remove').submit();
    }

    function updateItem(c_id, v_id) {
      $('#comp_id').val(c_id);
      $('#vc_id').val(v_id);
      $('#quantity').val($('#qty_'+v_id).val());

      $('#frm-update').submit();
    }

    $(function() {
      @forelse($carts as $key => $cart)
        $('#date_range_{{ $key }}').daterangepicker({
          opens: 'right',
          minDate:new Date(),
        }, function(start, end, label) {
          console.log("Date result {{ $key }} : " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
          $('#days_{{ $key }}').text(diffDays(start, end));
          $('#diff_days_{{ $key }}').val(diffDays(start, end));
          $('#start_date_{{ $key }}').val(start.format('YYYY-MM-DD'));
          $('#end_date_{{ $key }}').val(end.format('YYYY-MM-DD'));
        });
      @empty
        console.log('no data');
      @endforelse 
    });
    

    function diffDays(start, end) {
      var a = new Date(start),
      b = new Date(end),
      c = 24*60*60*1000,
      diffDays = Math.round(Math.abs((a - b)/(c)));
      return diffDays;
    }

    @if(Session::has('swal_notification.message'))
      var type = "{{ Session::get('swal_notification.level', 'info') }}";
      switch(type){
        case 'success':
          swal(
            'Sukses!',
            '{{ Session::get('swal_notification.message') }}',
            'success'
          );
          break;

        case 'error':
          swal(
            'Gagal!',
            '{{ Session::get('swal_notification.message') }}',
            'error'
          );
          break;
      }
    @endif
  </script>
@endsection