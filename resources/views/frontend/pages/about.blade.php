@extends('layouts.front')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/stisla/modules/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

@section('content')
  <section class="section">
    <div class="row">
      <div class="card col-md-8 mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">About</h6>
        </div>
        <div class="card-body">
          <h2><strong>Sejarah PO JMB Holiday</strong></h2>
          <p>Pariwisata merupakan suatu kegiatan perjalanan yang dilakukan untuk rekreasi atau liburan, dan juga persiapan yang dilakukan untuk aktivitas ini yang mana diperlukan oleh setiap orang. Bukan hanya bagi seorang individu atau kelompok saja yang memerlukannya, namun banyak negara yang bergantung dari industri ini sebagai sumber pajak dan pendapatan dalam meningkatkan pendapatan negara serta mendongkrak perekonomian rakyatnya.</p>
          <p>Contohnya untuk sebuah perusahaan yang menjual jasa kepada wisatawan sebagai sebuah peluang bisnis yang sangat menguntungkan untuk di jalani saat ini. Oleh karena itu, pengembangan industri pariwisata ini merupakan salah satu strategi yang dipakai oleh organisasi pemerintah serta non-pemerintah untuk mempromosikan wilayah tertentu sebagai daerah wisata untuk meningkatkan perdagangan melalui penjualan barang dan jasa kepada orang non-lokal.</p>
          <p>Persaingan yang ketat antar perusahaan otobus (PO) dan banyak pilihan membuat konsumen semakin selektif dalam memilih alternatif pilihan transportasi bus yang akan digunakan. Semakin berkembangnya usaha otobus di Jawa Jabarat menimbulkan banyak persaingan antar perusahaan otobus.&nbsp;</p>
          <p>Terdapat berbagai perusahaan yang bergerak di bidang pelayanan jasa pariwisata, salah satunya PO JMB Hoiday di bawah naungan PT.Titian insan Semesta.</p>
          <p>PO JMB Holiday dari sebuah usaha Biro Travel JMB yang&nbsp; dirintis pada tahun 2012 serta pada tahun 2014, Travel ini merintis sebuah usaha transportasi skala kecil dengan menyediakan transportasi mobil rental skala kecil dengan hanya mengandalkan satu unit dan dua unit bantuan dari keluarga.</p>
          <p>Kemudian tahun 2015 &nbsp;mengembangkan bisnis bidang transportasi pariwisata Perusahaan Otobus dengan membeli satu unit bus Pariwisata dan diberi nama PO JMB Holiday.</p>
          <p>Nama JMB &nbsp;yang dapat diartikan &ldquo;JAWAK MAJU BERSAMA&rdquo;. Nama ini melambangkan bahwa JAWAK itu merupakan sub marga pendirinya yaitu Hendry Liasta Ginting Jawak yang mempunyai impian buat maju bersama sama dengan orang-orang yang berada di dalam naungan JMB Holiday dan akan terus berkembang dan menjadi perusahaan besar di masa mendatang</p>
          <p>PO JMB Holiday memberi layanan pariwisata dengan kualitas terbaik, memberi kenyamanan dan keamanan penumpang. Untuk menambahkan kenyamanan selama perjalanan semua armada bus pariwisata PO JMB Holiday ini dilengkapi dengan fasilitas bantal, selimut dan wifi untuk menambah kenyamanan perjalanan anda.</p>
          <h2><strong>Visi dan Misi Perusahaan </strong></h2>
          <ul>
          <li>Visi
          <ul>
          <li>Menjadi perusahaan otobus yang peduli dan memberikan kesuksesan &nbsp;terhadap karyawan, lingkungan</li>
          <li>Memberi layanan transportasi yang berkualitas terbaik terhadap masyarakat.</li>
          </ul>
          </li>
          <li>Misi
          <ul>
          <li>Memberi kenyamanan dan keamanan penumpang dengan menggunakan armada bus yang terawatt dan terbaik</li>
          <li>Memberi kepuasan pelanggan dengan menetapkan kualitas pelayanan yang terbaik</li>
          </ul>
          </li>
          </ul>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card">
          <div class="card-header">
            <h6 class="m-0 font-weight-bold text-primary">Contact</h6>
          </div>
          <div class="card-body">
            <div class="form-group">
              <p>Email : jmb8holiday@gmail.com<br>IG : jmb8holiday<br>FB : jmb holiday<br>Telp/WA : 081221253634 / 085703483798</p>
<p><strong>Alamat</strong><br><strong>Kantor pusat :</strong> Komplek Taman Cibaduyut Indah Blok M No. 48 Bandung<br>(022)87777368/081289427704</p>
<p><strong>Kantor Cabang :</strong><br>Easton Park Residance Jl. Raya Jatinangor No. 73 Sumedang <br> 085220900055 / 081221253634</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

@endsection

@section('script')
  <script type="text/javascript" src="{{ asset('assets/stisla/modules/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
  <script>
    function numberFormat(x) {
      return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
    }

    $(document).ready(function () {
      bsCustomFileInput.init()
      $('.select2').select2();
    })

    function validate() {
      var valid = true;
			if (valid) {
					swal({
              title: "Konfirmasi",
              text: "Apakah anda yakin data yang diinput sudah benar?",
              icon: 'warning',
              buttons: {
              cancel: "Tidak",
              confirm: "Ya"
          }})
          .then((value) => {
              if(value == true){
                $('#form-payment').submit();
              }
          });
			}
		}

    @if(Session::has('swal_notification.message'))
      var type = "{{ Session::get('swal_notification.level', 'info') }}";
      switch(type){
        case 'success':
          swal(
            'Sukses!',
            '{{ Session::get('swal_notification.message') }}',
            'success'
          );
          break;

        case 'error':
          swal(
            'Gagal!',
            '{{ Session::get('swal_notification.message') }}',
            'error'
          );
          break;
      }
    @endif
  </script>
@endsection