@extends('layouts.front')

@section('css')
    <style>
      .param {
          margin-bottom: 7px;
          line-height: 1.4;
      }
      .param-inline dt {
          display: inline-block;
      }
      .param dt {
          margin: 0;
          margin-right: 7px;
          font-weight: 600;
      }
      .param-inline dd {
          vertical-align: baseline;
          display: inline-block;
      }

      .param dd {
          margin: 0;
          vertical-align: baseline;
      } 

      .shopping-cart-wrap .price {
          color: #007bff;
          font-size: 15px;
          font-weight: bold;
          margin-right: 5px;
          display: block;
      }
      var {
          font-style: normal;
      }

      .media img {
          margin-right: 1rem;
      }
      .img-sm {
          width: 90px;
          max-height: 75px;
          object-fit: cover;
      }
    </style>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/stisla/modules/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

@section('content')
  <section class="section">
    <div class="card mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Checkout - Pembayaran</h6>
      </div>
      <div class="card-body">
        
        <div class="row">
          <div class="col-md-8">
            <div class="card card-danger">
              <div class="card-header">
                Sewa ke : &nbsp;<strong>{{ $cart['company_name'] }}</strong>
              </div>
              <div class="card-body">
                <div class="row">
                  <form action="" id="frm-date" class="form-inline col-md-12" method="post">
                    <label class="mb-2">Tanggal Sewa</label>
                    <input type="text" class="form-control w-50 mb-2 mx-2" id="date_range" name="date_range" value="{{ $cart['start_date'] ?? date('Y-m-d') }} sd {{ $cart['end_date'] ?? date('Y-m-d') }}">
                  </form>
                </div>
                <table class="table table-bordered table-hover shopping-cart-wrap">
                  <thead class="text-muted">
                  <tr>
                    <th scope="col">Bus</th>
                    <th scope="col" width="150">Jumlah</th>
                    <th class="text-right" scope="col" width="180">Total Harga</th>
                  </tr>
                  </thead>
                    <tbody>
                      @php $subtotal = 0; $diff_days = $cart['diff_days'] ?? '1'; @endphp
                      @foreach ($cart['data'] as $item)
                        @php
                          $price_total = ((int) $item['price']) * $item['quantity'];
                          $subtotal += $price_total;
                        @endphp
                        <tr>
                          <td>
                            <figure class="media mt-2">
                              <div class="img-wrap"><img src="{{ $item['image_url'] }}" class="img-thumbnail img-sm"></div>
                              <figcaption class="media-body">
                                <h6 class="title text-truncate">{{ $item['name'] }}</h6>
                                <dl class="param param-inline small">
                                  <dt>Seat : </dt>
                                  <dd>{{ $item['seat'] }}</dd>
                                </dl>
                                <dl class="param param-inline small text-danger">
                                  <dt>Harga : </dt>
                                  <dd>Rp {{ rupiah((int) $item['price']) }}</dd>
                                </dl>
                              </figcaption>
                            </figure> 
                          </td>
                          <td> 
                            <select name="qty" id="qty_{{ $item['id'] }}" class="form-control" disabled>
                              @foreach (range(1, $item['available']) as $i)
                                <option value="{{ $i }}" {{ $item['quantity'] == $i ? 'selected' : '' }}>{{ $i }}</option>
                              @endforeach
                            </select>
                          </td>
                          <td class="text-right"> 
                            <div class="price-wrap"> 
                              <var class="price">Rp {{ rupiah($price_total) }}</var> 
                            </div>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                    <tfoot>
                      @if ($cart)
                        <tr>
                          <th class="text-right" colspan="2">Subtotal</th>
                          <td class="text-right">
                            <div class="price-wrap"> 
                              <var class="price">Rp {{ rupiah($subtotal) }}</var> 
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <th class="text-right" colspan="2">Lama Sewa</th>
                          <td class="text-right">
                            <div class="price-wrap"> 
                              <var class="price"><span id="days">{{ $diff_days }}</span> Hari</var> 
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <th class="text-right" colspan="2">Total</th>
                          <td class="text-right">
                            <div class="price-wrap"> 
                              <var class="price">Rp {{ rupiah($subtotal * $diff_days) }}</var> 
                            </div>
                          </td>
                        </tr>
                      @endif
                    </tfoot>
                  </table>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card card-danger">
              <div class="card-header">
                Pembayaran
              </div>
              <div class="card-body">
                <form id="frm-payment" action="{{ route('checkout.store', $company_id) }}" method="post">
                  @csrf
                  <div class="form-group {{ $errors->has('method') ? ' has-error' : '' }}">
                    <label for="method" class="d-block">Metode pembayaran</label>
                    <select name="method" class="form-control select2">
                      @foreach (config('app-data.payment_method') as $key => $item)
                        <option value="{{ $key }}">{{ $item }}</option>
                      @endforeach
                    </select>
                    @if ($errors->has('method'))
                      <div class="invalid-feedback">
                        <strong>{{ $errors->first('method') }}</strong>
                      </div>
                    @endif
                  </div>
                  <div class="form-group {{ $errors->has('notes') ? ' has-error' : '' }}">
                    <label for="notes" class="d-block">Catatan</label>
                    <input type="text" class="form-control" name="notes" value="{{ old('notes') }}">
                    @if ($errors->has('notes'))
                      <div class="invalid-feedback">
                        <strong>{{ $errors->first('notes') }}</strong>
                      </div>
                    @endif
                  </div>
                  <div class="form-group text-right">
                    <a href="javascript:void(0)" onclick="$('#frm-payment').submit()" class="btn btn-danger"><i class="fa fa-check"></i> Checkout</a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
            
      </div>
    </div>
  </section>

@endsection

@section('script')
  <script type="text/javascript" src="{{ asset('assets/stisla/modules/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
  <script>
    function numberFormat(x) {
      return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
    }

    function diffDays(start, end) {
      var a = new Date(start),
      b = new Date(end),
      c = 24*60*60*1000,
      diffDays = Math.round(Math.abs((a - b)/(c)));
      return diffDays;
    }

    @if(Session::has('swal_notification.message'))
      var type = "{{ Session::get('swal_notification.level', 'info') }}";
      switch(type){
        case 'success':
          swal(
            'Sukses!',
            '{{ Session::get('swal_notification.message') }}',
            'success'
          );
          break;

        case 'error':
          swal(
            'Gagal!',
            '{{ Session::get('swal_notification.message') }}',
            'error'
          );
          break;
      }
    @endif
  </script>
@endsection