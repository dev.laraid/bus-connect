@extends('layouts.front')

@section('content')
  {{-- <div class="col-12 mb-4">
    <div class="hero bg-primary text-white">
      <div class="hero-inner">
        <h2>Selamat datang di {{ config('app.name') }}</h2>
        <p class="lead">Solusi penyewaan bus sesama penyedia. Mudah, aman dan murah.</p>
      </div>
    </div>
  </div> --}}
  <section class="section">
    <h2 class="section-title">Layanan bus tersedia</h2>
    <div class="row">
      @forelse ($vehicles as $item)
        <div class="col-12 col-sm-6 col-md-6 col-lg-3">
          <article class="article article-style-b">
            <div class="article-header">
              <div class="article-image" data-background="{{ asset('uploads/images/vehicles/'.$item->image) }}">
              </div>
            </div>
            <div class="article-details">
              <div class="article-title">
                <h2><a href="javascript:void(0)" onclick="showDetail({{ $item->id }})">{{ $item->name }} | {{ $item->seat }} Seat</a></h2>
              </div>
              <strong class="text-danger">Rp {{ rupiah($item->price) }} -hari</strong> <br>
              <p>
                <i class="fa fa-fw fa-user"></i> <small>{{ $item->company->name }}</small> <br>
                <i class="fa fa-fw fa-map-marker-alt"></i> <small>{{ $item->company->location }}</small> <br>
              </p>
              <div class="article-cta">
                @if ($item->company_id != $company_id)  
                  <a href="javascript:void(0)" onclick="addCart({{ $item->id }})">Booking <i class="fas fa-chevron-right"></i></a>
                @else
                  <span>Owned <i class="fas fa-check"></i></span>
                @endif
              </div>
            </div>
          </article>
        </div>
      @empty
        <div class="col-md-2 offset-md-5">
          Tidak ada bus tersedia :(
        </div>
      @endforelse
      <div class="col-12">
        {{ $vehicles->links() }}
      </div>
    </div>
  </section>

    <!-- Modal -->
  <div class="modal fade" tabindex="-1" id="detailModal" role="dialog" aria-labelledby="detailModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Detail Bus</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          @csrf
          <div class="row">
            <div class="col-md-4">
              <img src="" class="rounded" id="image-vehicle" width="200" height="200" alt="image">
              <div class="mt-2">
                <strong class="text-danger txt-price">Rp 100000 -hari</strong> <br>
                <p>
                  <i class="fa fa-fw fa-user"></i> <span id="txt-company">aaaa</span> <br>
                  <i class="fa fa-fw fa-map-marker-alt"></i> <span id="txt-location">xxxx</span> <br>
                </p>
              </div>
            </div>
            <div class="col-md-8">
              <strong>Deksripsi</strong>
              <div id="txt-description">

              </div>
            </div>
          </div>
          
        </div>
        <div class="modal-footer">
          <a href="javascript:void(0)" type="button" onclick="addCart()" id="btn-a2c" class="btn btn-primary">Booking</a>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <form id="booking-form" action="{{ route('cart.add') }}" method="POST" style="display: none;">
      @csrf
      <input type="hidden" id="bus-id" name="bus_id">
  </form>
@endsection

@section('script')
  <script>
    function showDetail(id) {
      $.ajax({
         type:'POST',
         url:'{{ route('vehicle.detail') }}',
         headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()},
         data: {id:id},
         success:function(data){
            console.log(data)

            $('.txt-price').text('Rp ' + numberFormat(parseInt(data.price)) + ' -hari');
            $('#txt-company').text(data.company.name);
            $('#txt-location').text(data.company.location);
            $('#txt-description').html(data.description);
            $('#bus-id').val(data.id);

            var img = '{{ asset('uploads/images/vehicles') }}/' + data.image;
            if (img != null) {
              $('#image-vehicle').attr('src', img);
            }

            if (data.company_id == '{{ $company_id }}') {
              $('#btn-a2c').hide();
            } else {
              $('#btn-a2c').show();
            }

            $('#detailModal').modal('show');
         }
      });
    }

    function addCart(params = null) {
      if(params != null) {
        $('#bus-id').val(params);
      }
      $('#booking-form').submit();
    }

    function numberFormat(x) {
      return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
    }

    @if(Session::has('swal_notification.message'))
      var type = "{{ Session::get('swal_notification.level', 'info') }}";
      switch(type){
        case 'success':
          swal(
            'Sukses!',
            '{{ Session::get('swal_notification.message') }}',
            'success'
          );
          break;

        case 'error':
          swal(
            'Gagal!',
            '{{ Session::get('swal_notification.message') }}',
            'error'
          );
          break;
      }
    @endif
  </script>
@endsection