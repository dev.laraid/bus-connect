@extends('layouts.front')

@section('content')
  <section class="section">
    <div class="section-header">
      <h1>Dashboard</h1>
    </div>

    <div class="section-body">
      @php
        $company_id = \Auth::user()->company->id;
      @endphp
      <div class="row">

        <div class="col-lg-4 col-md-4 col-sm-6 col-12">
          <div class="card card-statistic-1">
            <div class="card-icon bg-primary">
              <i class="fas fa-bus"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Total Bus</h4>
              </div>
              <div class="card-body">
                {{ $data['vehicle'] }}
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-6 col-12">
          <div class="card card-statistic-1">
            <div class="card-icon bg-success">
              <i class="fas fa-receipt"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Total Transaksi Masuk</h4>
              </div>
              <div class="card-body">
                {{ $data['booking_in'] }}
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-6 col-12">
          <div class="card card-statistic-1">
            <div class="card-icon bg-warning">
              <i class="fas fa-receipt"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Total Transaksi Keluar</h4>
              </div>
              <div class="card-body">
                {{ $data['booking_out'] }}
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
          <div class="card card-statistic-1">
            <div class="card-icon bg-info">
              <i class="fas fa-dollar-sign"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Saldo Tersimpan</h4>
              </div>
              <div class="card-body">
                Rp {{ rupiah($data['balances']) }}
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
          <div class="card card-statistic-1">
            <div class="card-icon bg-danger">
              <i class="fas fa-dollar-sign"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Saldo Dicairkan</h4>
              </div>
              <div class="card-body">
                Rp {{ rupiah($data['payouts']) }}
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-12">

          <!-- Basic Card Example -->
          <div class="card mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Selamat datang</h6>
            </div>
            <div class="card-body">
              
              @if (session('status'))
                  <div class="alert alert-primary" role="alert">
                      {{ session('status') }}
                  </div>
              @endif
              Anda login sebagai <i>{{Auth::user()->name}}</i> <br>
            
            </div>
          </div>
        </div>
      </div>

    </div>
  </section>
@endsection