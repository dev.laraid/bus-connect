@extends('layouts.front')

@section('css')
    <style>
      .param {
          margin-bottom: 7px;
          line-height: 1.4;
      }
      .param-inline dt {
          display: inline-block;
      }
      .param dt {
          margin: 0;
          margin-right: 7px;
          font-weight: 600;
      }
      .param-inline dd {
          vertical-align: baseline;
          display: inline-block;
      }

      .param dd {
          margin: 0;
          vertical-align: baseline;
      } 

      .shopping-cart-wrap .price {
          color: #007bff;
          font-size: 15px;
          font-weight: bold;
          margin-right: 5px;
          display: block;
      }
      var {
          font-style: normal;
      }

      .media img {
          margin-right: 1rem;
      }
      .img-sm {
          width: 90px;
          max-height: 75px;
          object-fit: cover;
      }
    </style>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/stisla/modules/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

@section('content')
  <section class="section">
    <div class="card mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Detail Transaksi - Pembatalan</h6>
      </div>
      <div class="card-body">
        
        <div class="row">
          <div class="col-md-8">
            <div class="card card-danger">
              <div class="card-header">
                PO yang menyewa : &nbsp;<strong>{{ $booking->customer->company->name }}</strong>
              </div>
              <div class="card-body">
                <div class="row">
                  <form action="" id="frm-date" class="form-inline col-md-12" method="post">
                    <label class="mb-2">Tanggal Sewa</label>
                    <input type="text" class="form-control w-50 mb-2 mx-2" id="date_range" name="date_range" value="{{ $booking->start_date ?? date('Y-m-d') }} sd {{ $booking->end_date ?? date('Y-m-d') }}" disabled>
                  </form>
                </div>
                <table class="table table-bordered table-hover shopping-cart-wrap">
                  <thead class="text-muted">
                  <tr>
                    <th scope="col">Bus</th>
                    <th scope="col" width="150">Jumlah</th>
                    <th class="text-right" scope="col" width="180">Total Harga</th>
                  </tr>
                  </thead>
                    <tbody>
                      @php $subtotal = 0; @endphp
                      @foreach ($booking->details as $item)
                        @php
                          $price_total = ((int) $item->price) * $item->quantity;
                          $subtotal += $price_total;
                        @endphp
                        <tr>
                          <td>
                            <figure class="media mt-2">
                              <div class="img-wrap"><img src="{{ asset('uploads/images/vehicles/'. $item->vehicle->image) }}" class="img-thumbnail img-sm"></div>
                              <figcaption class="media-body">
                                <h6 class="title text-truncate">{{ $item->name }}</h6>
                                <dl class="param param-inline small">
                                  <dt>Seat : </dt>
                                  <dd>{{ $item->seat }}</dd>
                                </dl>
                                <dl class="param param-inline small text-danger">
                                  <dt>Harga : </dt>
                                  <dd>Rp {{ rupiah((int) $item->price) }}</dd>
                                </dl>
                              </figcaption>
                            </figure> 
                          </td>
                          <td> 
                            <input type="text" value="{{ $item->quantity }}" class="form-control" disabled>
                          </td>
                          <td class="text-right"> 
                            <div class="price-wrap"> 
                              <var class="price">Rp {{ rupiah($price_total) }}</var> 
                            </div>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                    <tfoot>
                      @if ($booking)
                        <tr>
                          <th class="text-right" colspan="2">Subtotal</th>
                          <td class="text-right">
                            <div class="price-wrap"> 
                              <var class="price">Rp {{ rupiah($subtotal) }}</var> 
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <th class="text-right" colspan="2">Lama Sewa</th>
                          <td class="text-right">
                            <div class="price-wrap"> 
                              <var class="price"><span id="days">{{ $diff_days }}</span> Hari</var> 
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <th class="text-right" colspan="2">Total</th>
                          <td class="text-right">
                            <div class="price-wrap"> 
                              <var class="price">Rp {{ rupiah($subtotal * $diff_days) }}</var> 
                            </div>
                          </td>
                        </tr>
                      @endif
                    </tfoot>
                  </table>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card card-danger">
              <div class="card-header">
                Info
              </div>
              <div class="card-body">
                <form id="frm-payment" action="" method="post">
                  @csrf
                  <div class="form-group {{ $errors->has('method') ? ' has-error' : '' }}">
                    <label for="method" class="d-block">Metode pembayaran</label>
                    <select name="method" class="form-control select2" disabled>
                      @foreach (config('app-data.payment_method') as $key => $item)
                        <option value="{{ $key }}" {{ $booking->payment_method == $key ? 'selected' : '' }}>{{ $item }}</option>
                      @endforeach
                    </select>
                    @if ($errors->has('method'))
                      <div class="invalid-feedback">
                        <strong>{{ $errors->first('method') }}</strong>
                      </div>
                    @endif
                  </div>
                  <div class="form-group {{ $errors->has('notes') ? ' has-error' : '' }}">
                    <label for="notes" class="d-block">Catatan</label>
                    <input type="text" class="form-control" name="notes" value="{{ $booking->notes }}" disabled>
                    @if ($errors->has('notes'))
                      <div class="invalid-feedback">
                        <strong>{{ $errors->first('notes') }}</strong>
                      </div>
                    @endif
                  </div>
                  <div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
                    <label for="status" class="d-block">Status</label>
                    <input type="text" class="form-control" name="status" value="{{ $booking->display_status }}" disabled>
                    @if ($errors->has('status'))
                      <div class="invalid-feedback">
                        <strong>{{ $errors->first('status') }}</strong>
                      </div>
                    @endif
                  </div>
                </form>
              </div>
            </div>
            @if (in_array($booking->status, [100,200])) 
              <div class="card card-danger">
                <div class="card-header">
                  Form Pembatalan
                </div>
                <div class="card-body">
                  <form id="frm-reject" action="{{ route('transaction_in.update', $booking->id) }}" method="post">
                    @csrf
                    @method('patch')
                    <div class="form-group {{ $errors->has('reject_reason') ? ' has-error' : '' }}">
                      <label for="reject_reason" class="d-block">Alasan membatalkan</label>
                      <textarea name="reject_reason" class="form-control @if ($errors->has('reject_reason')) is-invalid @endif"  cols="30" rows="10">{{ old('reject_reason') }}</textarea>
                      @if ($errors->has('reject_reason'))
                        <div class="invalid-feedback">
                          <strong>{{ $errors->first('reject_reason') }}</strong>
                        </div>
                      @endif
                    </div>
                    <button class="btn btn-danger btn-block">Batalkan Booking Ini</button>
                  </form>
                </div>
              </div>
            @endif

            @if ($booking->status == 10)
              <div class="alert alert-danger alert-has-icon">
                <div class="alert-icon"><i class="fa fa-info"></i></div>
                <div class="alert-body">
                  <div class="alert-title">Booking Dibatalkan</div>
                  <strong>Alasan :</strong> <br> {{ $booking->reject_reason }}
                </div>
              </div>
            @endif
          </div>
        </div>
            
      </div>
    </div>
  </section>

@endsection

@section('script')
  <script type="text/javascript" src="{{ asset('assets/stisla/modules/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
  <script>
    function numberFormat(x) {
      return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
    }

    function diffDays(start, end) {
      var a = new Date(start),
      b = new Date(end),
      c = 24*60*60*1000,
      diffDays = Math.round(Math.abs((a - b)/(c)));
      return diffDays;
    }

    @if(Session::has('swal_notification.message'))
      var type = "{{ Session::get('swal_notification.level', 'info') }}";
      switch(type){
        case 'success':
          swal(
            'Sukses!',
            '{{ Session::get('swal_notification.message') }}',
            'success'
          );
          break;

        case 'error':
          swal(
            'Gagal!',
            '{{ Session::get('swal_notification.message') }}',
            'error'
          );
          break;
      }
    @endif
  </script>
@endsection