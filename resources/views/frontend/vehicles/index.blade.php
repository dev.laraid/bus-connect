@extends('layouts.front')

@section('content')
  <section class="section">
    <div class="section-header">
      <h1>Bus</h1>
    </div>

    <div class="d-sm-flex align-items-center justify-content-start mb-4">
      <a class="btn btn-sm btn-danger mr-auto" href="{{ route('vehicle.create') }}"><i class="fa fa-plus"></i> Tambah Bus</a> 
      <div class="form-inline">
        <label>Filter Status</label>
        <select name="status" class="form-control-sm ml-2">
          <option value="all">Semua</option>
          <option value="100">Aktif</option>
          <option value="10">Tidak Aktif</option>
        </select>
        <button class="btn btn-sm btn-danger ml-2" id="btn-filter"><i class="fas fa-filter"></i></button>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">

        <!-- Basic Card Example -->
        <div class="card card-danger">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">List Bus</h6>
          </div>
          <div class="card-body">

          <div class="table-responsive">
            <table class="table table-striped datatable">
              <thead>                                 
                <tr>
                  <th>#</th>
                  <th>Nama</th>
                  <th>Jml. Kursi</th>
                  <th>Jml. Tersedia</th>
                  <th>Harga (per-1 hari)</th>
                  <th>Status</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>

          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
@section('script')
<script>
  $(document).ready(function() {
      $('.datatable').DataTable({
          processing: true,
          serverSide: true,
          autoWidth: false,
          language: {
              url: '{{ asset('assets/stisla/modules/datatables/lang/Indonesian.json') }}'
          },
          ajax: {
            url: '{{ route('vehicle.index') }}',
            data: function (d) {
              d.status = $('select[name=status]').val()
            }
          },
          columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'name', name: 'name'},
            {data: 'seat', name: 'seat'},
            {data: 'available', name: 'available'},
            {data: null, name: 'price', render: function ( data, type, row ) {
              return 'Rp '+numberFormat(data.price)+'</span>';
            }},
            {data: 'display_status', name: 'status', orderable: false, searchable: false},
            {data: 'action', name: 'action', orderable: false, searchable: false},
          ]
      });

      $('#btn-filter').click(function(){
         $('.datatable').DataTable().draw(true);
      });

      $(document).on('click','.js-submit-confirm', function(e){
          e.preventDefault();
          swal({
            title: 'Apakah anda yakin ingin menghapus?',
            text: 'Data yang sudah dihapus, tidak dapat dikembalikan!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              $(this).closest('form').submit();
            } 
          });
      });
  });

  function numberFormat(x) {
    return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
  }

</script>
@endsection
