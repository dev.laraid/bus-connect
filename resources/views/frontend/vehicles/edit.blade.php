@extends('layouts.front')

@section('css')
  <link rel="stylesheet" href="{{ asset('assets/stisla/modules/summernote/summernote-bs4.css') }}">
@endsection

@section('content')
  <section class="section">
    <div class="section-header">
      <h1>Ubah Bus</h1>
    </div>

    <form method="POST" action="{{ route('vehicle.update', $vehicle->id) }}" enctype="multipart/form-data">
      <div class="row">
        <div class="col-lg-8">
          <div class="card card-danger">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-b$vehicle->">Informasi Bus</h6>
            </div>
            <div class="card-body">
              {{ csrf_field() }}
              {{ method_field('PUT') }}

              <div class="row">
                <div class="form-group col-8 {{ $errors->has('name') ? ' has-error' : '' }}">
                  <label for="name">Nama Bus</label>
                  <input id="name" type="text" class="form-control @if ($errors->has('name')) is-invalid @endif" name="name" value="{{ $vehicle->name }}">
                  @if ($errors->has('name'))
                    <div class="invalid-feedback">
                      {{ $errors->first('name') }}
                    </div>
                  @endif
                </div>
                
                <div class="form-group col-4 {{ $errors->has('seat') ? ' has-error' : '' }}">
                  <label for="seat">Kapasitas Kursi</label>
                  <input id="seat" type="number" class="form-control @if ($errors->has('seat')) is-invalid @endif" name="seat" value="{{ $vehicle->seat }}">
                  @if ($errors->has('seat'))
                    <div class="invalid-feedback">
                      {{ $errors->first('seat') }}
                    </div>
                  @endif
                </div>
              </div>

              <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                <label for="description">Deskripsi Bus</label>
                <textarea id="description" class="summernote @if ($errors->has('description')) is-invalid @endif" name="description">{!! $vehicle->description !!}</textarea>
                @if ($errors->has('description'))
                  <div class="invalid-feedback">
                    {{ $errors->first('description') }}
                  </div>
                @endif
              </div>
              
              <div class="row">
                <div class="form-group col-4 {{ $errors->has('available') ? ' has-error' : '' }}">
                  <label for="available" class="d-block">Jumlah Bus Tersedia</label>
                  <input id="available" type="number" class="form-control @if ($errors->has('available')) is-invalid @endif" value="{{ $vehicle->available }}" name="available">
                  @if ($errors->has('available'))
                    <div class="invalid-feedback">
                      <strong>{{ $errors->first('available') }}</strong>
                    </div>
                  @endif
                </div>
                <div class="form-group col-8 {{ $errors->has('price') ? ' has-error' : '' }}">
                  <label for="price" class="d-block">Harga Sewa (per-1 hari)</label>
                  <input id="price" type="number" class="form-control @if ($errors->has('price')) is-invalid @endif" value="{{ (int) $vehicle->price }}" name="price">
                  @if ($errors->has('price'))
                    <div class="invalid-feedback">
                      <strong>{{ $errors->first('price') }}</strong>
                    </div>
                  @endif
                </div>
              </div>

              <div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
                <label for="status" class="d-block">Status</label>
                <select name="status" class="form-control select2">
                  <option value="100" {{ ($vehicle->status == 100) ? 'selected' : '' }}>Aktif</option>
                  <option value="10" {{ ($vehicle->status == 10) ? 'selected' : '' }}>Tidak Aktif</option>
                </select>
                @if ($errors->has('status'))
                  <div class="invalid-feedback">
                    <strong>{{ $errors->first('status') }}</strong>
                  </div>
                @endif
              </div>

              <div class="form-group">
                <button type="submit" class="btn btn-danger btn-block" tabindex="4">
                  Simpan
                </button>
              </div>
              
            </div>
          </div>
        </div>

        <div class="col-lg-4">
          <div class="card card-danger">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-b$vehicle->">Foto Bus</h6>
            </div>
            <div class="card-body">
              <div class="form-group">
                <div class="text-center">
                  @if (is_null($vehicle->image))
                    <img src="{{ asset('assets/stisla/img/example-image.jpg') }}" class="rounded" id="image-prev" width="200" height="200" alt="image">
                  @else
                    <img src="{{asset('uploads/images/vehicles/'.$vehicle->image)}}" class="rounded" id="image-prev" width="200" height="200" alt="image">
                  @endif
                </div>
              </div>
              <div class="form-group custom-file mb-3">
                <input id="image" type="file" class="custom-file-input {{ $errors->has('image') ? ' has-error' : '' }}" name="image">
                <label class="custom-file-label" for="customFile">Pilih Gambar</label>
              </div>
              @if ($errors->has('image'))
                <div class="invalid-feedback">
                  <strong>{{ $errors->first('image') }}</strong>
                </div>
              @endif
            </div>
          </div>
        </div>
      </div>
    </form>
  </section>
@endsection

@section('script')
  <script src="{{ asset('assets/stisla/modules/summernote/summernote-bs4.js') }}"></script>
  <script>
     $(".summernote").summernote({
        dialogsInBody: true,
        minHeight: 150,
        toolbar: [
          ['style', ['style']],
          ['font', ['b$vehicle->', 'underline', 'clear']],
          ['fontname', ['fontname']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['view', ['codeview', 'help']],
        ],
    });

    $(document).ready(function () {
      bsCustomFileInput.init()
      $('.select2').select2();
    })

    function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('#image-prev').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
    }

    $("#image").change(function() {
      readURL(this);
    });
  </script>
@endsection