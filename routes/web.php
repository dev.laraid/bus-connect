<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Frontend\PageController@landing')->name('landing');
Route::get('/about', 'Frontend\PageController@pageAbout')->name('page.about');
Route::post('/vehicle/detail', 'Frontend\PageController@showVehicle')->name('vehicle.detail');

Route::group(['middleware' => ['auth', 'role:member']], function () {
    Route::get('/booking/cart', 'Frontend\PageController@cart')->name('cart.index');
    Route::get('/booking/{company_id}/checkout', 'Frontend\PageController@checkout')->name('checkout');
    Route::get('/booking/{invoice}/confirm', 'Frontend\PageController@confirm')->name('confirm');
    Route::post('/booking/{invoice}/confirm', 'Frontend\PageController@confirmStore')->name('confirm.store');
    Route::post('/booking/{company_id}/checkout', 'Frontend\PageController@checkoutStore')->name('checkout.store');
    Route::post('/booking/cart', 'Frontend\PageController@addToCart')->name('cart.add');
    Route::post('/booking/cart/remove', 'Frontend\PageController@removeItemCart')->name('cart.remove');
    Route::post('/booking/cart/update', 'Frontend\PageController@updateCart')->name('cart.update');
    Route::post('/booking/cart/dateadd', 'Frontend\PageController@addDateCart')->name('cart.dateadd');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/account/profile', 'ProfileController@show')->name('profile.show');
Route::put('/account/profile', 'ProfileController@update')->name('profile.update');
Route::put('/account/profile/member', 'ProfileController@updateMember')->name('profile.update.member');

Route::group(['prefix' => 'main', 'middleware' => ['auth', 'role:admin']], function () {
    Route::get('user', 'Backend\UserController@index' )->name('user.index');
    Route::post('user', 'Backend\UserController@store' )->name('user.store');
    Route::get('user/create', 'Backend\UserController@create' )->name('user.create');
    Route::get('user/{id}/edit', 'Backend\UserController@edit' )->name('user.edit');
    Route::put('user/{id}/update', 'Backend\UserController@update' )->name('user.update');
    Route::delete('user/{id}', 'Backend\UserController@destroy' )->name('user.destroy');

    Route::resource('booking','Backend\BookingController', ['only' => ['index', 'show', 'update']]);
    Route::resource('payment','Backend\PaymentController', ['only' => ['index', 'show', 'update']]);
    Route::resource('company','Backend\CompanyController', ['only' => ['index', 'show']]);
    Route::post('company.verification', 'Backend\CompanyController@verification')->name('company.verification');
    Route::resource('payouts', 'Backend\PayoutController', ['only' => ['index', 'show', 'update']]);
});

Route::group(['prefix' => 'intern', 'middleware' => ['auth', 'role:member']], function () {
    Route::resource('vehicle', 'Frontend\VehicleController');
    Route::resource('payout','Frontend\PayoutController', ['only' => ['index', 'show', 'create', 'store']]);
    Route::resource('transaction_in', 'Frontend\TransInController', ['only' => ['index', 'show', 'edit', 'update']]);
    Route::resource('transaction_out', 'Frontend\TransOutController', ['only' => ['index', 'show']]);
});