/*
 Navicat Premium Data Transfer

 Source Server         : laragon-mysql
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : bus_connect

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 20/07/2020 01:17:56
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for booking_details
-- ----------------------------
DROP TABLE IF EXISTS `booking_details`;
CREATE TABLE `booking_details`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `booking_id` bigint(20) UNSIGNED NOT NULL,
  `vehicle_id` bigint(20) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(10, 2) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of booking_details
-- ----------------------------
INSERT INTO `booking_details` VALUES (1, 1, 3, 1, 2800000.00, '2020-07-01 16:27:07', '2020-07-01 16:27:07', NULL);
INSERT INTO `booking_details` VALUES (2, 1, 4, 2, 4000000.00, '2020-07-01 16:27:07', '2020-07-01 16:27:07', NULL);
INSERT INTO `booking_details` VALUES (3, 2, 7, 2, 3500000.00, '2020-07-01 16:40:02', '2020-07-01 16:40:02', NULL);
INSERT INTO `booking_details` VALUES (4, 3, 1, 1, 2800000.00, '2020-07-02 15:10:12', '2020-07-02 15:10:12', NULL);
INSERT INTO `booking_details` VALUES (5, 3, 5, 2, 4000000.00, '2020-07-02 15:10:12', '2020-07-02 15:10:12', NULL);

-- ----------------------------
-- Table structure for bookings
-- ----------------------------
DROP TABLE IF EXISTS `bookings`;
CREATE TABLE `bookings`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `invoice` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` bigint(20) NULL DEFAULT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `notes` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `vehicle_total` int(11) NOT NULL DEFAULT 0,
  `price_total` decimal(12, 2) NOT NULL DEFAULT 0.00,
  `payment_method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `is_paid` tinyint(1) NOT NULL DEFAULT 0,
  `status` int(11) NOT NULL DEFAULT 100,
  `reject_reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bookings
-- ----------------------------
INSERT INTO `bookings` VALUES (1, '5EFCB95BDE7FE', 2, 2, '2020-07-16', '2020-07-17', 'tolong baca', 3, 21600000.00, 'bca', 0, 100, NULL, '2020-07-01 16:27:07', '2020-07-01 16:27:07', NULL);
INSERT INTO `bookings` VALUES (2, '5EFCBC626AAF3', 3, 2, '2020-07-17', '2020-07-18', 'msntap', 2, 14000000.00, 'mandiri', 1, 300, NULL, '2020-07-01 16:40:02', '2020-07-13 13:23:14', NULL);
INSERT INTO `bookings` VALUES (3, '5EFDF8D4C6D41', 1, 3, '2020-07-16', '2020-07-24', 'tolong baca', 3, 97200000.00, 'bca', 1, 300, 'Mohon maaf bus pada tanggal sekian tidak tersedia.', '2020-07-02 15:10:12', '2020-07-18 15:21:03', NULL);

-- ----------------------------
-- Table structure for companies
-- ----------------------------
DROP TABLE IF EXISTS `companies`;
CREATE TABLE `companies`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `owner_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `balances` decimal(12, 2) NOT NULL DEFAULT 0.00,
  `bank_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `bank_account` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `bank_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `is_verification` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of companies
-- ----------------------------
INSERT INTO `companies` VALUES (1, 2, 'PO. Patriot Pariwisata', '<p>Kami menyediakan paket full service penyewaan bus</p><p>Aman, Terpercaya.</p>', 'Antapani - Bandung', 97200000.00, NULL, NULL, NULL, '5efa0367e9cb9.jpg', 1, '2020-06-07 18:33:21', '2020-07-19 17:28:32', NULL);
INSERT INTO `companies` VALUES (2, 3, 'PO. JMB Holiday', 'Kami menyediakan penyewaan termurah se-indonesia<p></p>', 'Pasteur - Bandung', 0.00, NULL, NULL, NULL, '5ef9e1178971b.jpg', 1, '2020-06-29 14:55:01', '2020-07-18 15:23:04', NULL);
INSERT INTO `companies` VALUES (3, 4, 'PO Putra Jaya Mandiri', 'Kami menyediakan bus premium dengan harga minimum<p></p>', 'Cibiru - Bandung', 0.00, 'BANK MANDIRI', 'Asep Soliin', '1892888918000', '5efb471de213b.jpeg', 1, '2020-06-30 14:06:38', '2020-07-19 18:17:30', NULL);

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (8, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (9, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (10, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (11, '2020_06_06_101708_create_companies_table', 1);
INSERT INTO `migrations` VALUES (12, '2020_06_06_101741_create_vehicles_table', 1);
INSERT INTO `migrations` VALUES (13, '2020_06_06_101756_create_bookings_table', 1);
INSERT INTO `migrations` VALUES (14, '2020_06_06_102113_create_payments_table', 1);
INSERT INTO `migrations` VALUES (15, '2020_07_13_123939_alter_table_booking_add_some_field', 2);
INSERT INTO `migrations` VALUES (16, '2020_07_13_141128_alter_table_all_add_soft_deletes', 3);
INSERT INTO `migrations` VALUES (18, '2020_07_18_134111_create_payouts_table', 4);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for payments
-- ----------------------------
DROP TABLE IF EXISTS `payments`;
CREATE TABLE `payments`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `booking_id` bigint(20) UNSIGNED NOT NULL,
  `method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_holder` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `paid` decimal(12, 2) NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `is_verification` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of payments
-- ----------------------------
INSERT INTO `payments` VALUES (1, 2, 'BANK MANDIRI', 'Ujang Maman', 14000000.00, '5efddf5c68b1d.jpg', 1, '2020-07-02 13:21:32', '2020-07-02 14:56:17', NULL);
INSERT INTO `payments` VALUES (2, 3, 'BANK MANDIRI', 'Puspito', 97200000.00, '5efdf8ee0a293.jpg', 1, '2020-07-02 15:10:38', '2020-07-18 15:20:35', NULL);

-- ----------------------------
-- Table structure for payouts
-- ----------------------------
DROP TABLE IF EXISTS `payouts`;
CREATE TABLE `payouts`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) UNSIGNED NOT NULL,
  `admin_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `nominal` bigint(20) UNSIGNED NOT NULL,
  `admin_fee` bigint(20) UNSIGNED NOT NULL,
  `payout_total` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `is_payout` tinyint(1) NOT NULL DEFAULT 0,
  `status` int(11) NOT NULL DEFAULT 100,
  `reject_reason` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of payouts
-- ----------------------------
INSERT INTO `payouts` VALUES (1, 3, 1, 14000000, 700000, 13300000, '5f148c2cd0214.jpg', 1, 200, NULL, '2020-07-19 17:25:02', '2020-07-19 18:08:44', NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(18) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `identity_card` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `role` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'member',
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Administrator', 'admin@gmail.com', '08199601000', '3207089765341678', NULL, '$2y$10$4CvyY1yc8dwoK6RQK4ijP.lQB19Lh5D5jU6xY0Ou37dvgapqtw1xq', '5edcc760e5c54.jpg', 'admin', NULL, '2020-06-07 04:55:50', '2020-06-07 10:54:24', NULL);
INSERT INTO `users` VALUES (2, 'Ujang Maman', 'ujang@gmail.com', '089666675280', '3209123456799879', NULL, '$2y$10$A3OIuMLQLfylvFlCr11Z0OWviJEBo6j5q8iq0OZbSWkirZSY8Z4XW', '5edca8d429e8e.jpg', 'member', NULL, '2020-06-07 08:24:03', '2020-07-02 13:51:49', NULL);
INSERT INTO `users` VALUES (3, 'Siswo Puspito', 'puspitosiswo@gmail.com', '089691100100', '3209123456799901', NULL, '$2y$10$sXyfyyUEkAQpkrwVZnXzVOAI6WFncnsQdiWojZlUN03nViBKsCz26', '5efa0367e710f.jpg', 'member', NULL, '2020-06-29 14:55:01', '2020-06-29 15:06:15', NULL);
INSERT INTO `users` VALUES (4, 'Asep Solihin', 'asep@gmail.com', '08969286578', '3209123456711331', NULL, '$2y$10$p3YiDxwebvxSnSlHDBHvSe2CRn.hiSUsJZrwx58cs.NtYWZawwHxy', '5efb471dde53e.jpg', 'member', NULL, '2020-06-30 14:06:38', '2020-07-13 14:31:49', NULL);

-- ----------------------------
-- Table structure for vehicles
-- ----------------------------
DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE `vehicles`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `seat` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `available` int(11) NOT NULL DEFAULT 1,
  `price` decimal(10, 2) NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 100,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of vehicles
-- ----------------------------
INSERT INTO `vehicles` VALUES (1, 1, 'Jetbus HD JMB', '50', '<ol><li>Air Conditioner (AC)</li><li>LCD TV</li><li>Audio System & DVD Player</li><li>Microphone</li><li>Coolerbox</li><li>Charger Port</li><li>Recleaning Seat</li><li>Bagasi Luar Luas</li><li>Bagasi Dalam Tertutup</li><li>Bantal & Selimut</li></ol>', 5, 2800000.00, '5ef9ebaf03614.jpg', 100, '2020-06-29 13:25:03', '2020-06-29 15:17:06', NULL);
INSERT INTO `vehicles` VALUES (2, 2, 'Jetbus 3 Patriot', '59', '<ol><li>Air Conditioner (AC)</li><li>LCD TV</li><li>Audio System&nbsp;</li><li>Charger Port</li><li>Recleaning Seat</li><li>Bagasi Luar Luas</li><li>Bagasi Dalam Tertutup</li><li>Bantal &amp; Selimut</li></ol>', 4, 3000000.00, '5efa0436f23c4.jpg', 100, '2020-06-29 15:09:42', '2020-06-29 15:09:42', NULL);
INSERT INTO `vehicles` VALUES (3, 2, 'Jetbus 2 Patriot', '50', '<ol><li>Air Conditioner (AC)</li><li>LCD TV</li><li>Audio System&nbsp;</li><li>Charger Port</li><li>Recleaning Seat</li><li>Bagasi Luar Luas</li><li>Bagasi Dalam Tertutup</li><li>Bantal &amp; Selimut</li></ol>', 6, 2800000.00, '5efa05919cdff.jpg', 100, '2020-06-29 15:15:29', '2020-06-29 15:15:29', NULL);
INSERT INTO `vehicles` VALUES (4, 2, 'Jetbus 1 Patriot', '64', '<ol><li>Air Conditioner (AC)</li><li>LCD TV</li><li>Audio System&nbsp;</li><li>Charger Port</li><li>Recleaning Seat</li><li>Bagasi Luar Luas</li><li>Bagasi Dalam Tertutup</li><li>Bantal &amp; Selimut</li></ol>', 3, 4000000.00, '5efa05d18ba67.jpg', 100, '2020-06-29 15:16:33', '2020-06-29 15:16:33', NULL);
INSERT INTO `vehicles` VALUES (5, 1, 'Jetbus 2+ HDD JMB', '64', '<ol><li>Air Conditioner (AC)</li><li>LCD TV</li><li>Audio System &amp; DVD Player</li><li>Microphone</li><li>Coolerbox</li><li>Charger Port</li><li>Recleaning Seat</li><li>Bagasi Luar Luas</li><li>Bagasi Dalam Tertutup</li><li>Bantal &amp; Selimut</li></ol>', 4, 4000000.00, '5efa06522774f.jpg', 100, '2020-06-29 15:18:42', '2020-06-29 15:18:42', NULL);
INSERT INTO `vehicles` VALUES (6, 1, 'Jetbus HD Mustika Jaya JMB', '59', '<ol><li>Air Conditioner (AC)</li><li>LCD TV</li><li>Audio System &amp; DVD Player</li><li>Microphone</li><li>Coolerbox</li><li>Charger Port</li><li>Recleaning Seat</li><li>Bagasi Luar Luas</li><li>Bagasi Dalam Tertutup</li><li>Bantal &amp; Selimut</li></ol>', 5, 3200000.00, '5efb4647a398d.jpg', 100, '2020-06-30 14:03:51', '2020-07-13 14:40:32', NULL);
INSERT INTO `vehicles` VALUES (7, 3, 'Jetbus 3 PJM', '59', '<ol><li>Air Conditioner (AC)</li><li>LCD TV</li><li>Audio System&nbsp;</li><li>Charger Port</li><li>Recleaning Seat</li><li>Bagasi Luar Luas</li><li>Bagasi Dalam Tertutup</li><li>Bantal &amp; Selimut</li></ol>', 4, 3500000.00, '5efb48de70348.jpg', 100, '2020-06-30 14:14:54', '2020-06-30 14:14:54', NULL);

SET FOREIGN_KEY_CHECKS = 1;
