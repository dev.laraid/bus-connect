<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payouts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('admin_id')->nullable();
            $table->unsignedBigInteger('nominal');
            $table->unsignedBigInteger('admin_fee');
            $table->unsignedBigInteger('payout_total');
            $table->string('image')->nullable();
            $table->boolean('is_payout')->default(0);
            $table->integer('status')->default(100);
            $table->text('reject_reason')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('companies', function (Blueprint $table) {
            $table->string('bank_name')->after('balances')->nullable();
            $table->string('bank_account')->after('bank_name')->nullable();
            $table->string('bank_number')->after('bank_account')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payouts');
        Schema::table('companies', function (Blueprint $table) {
            $table->dropColumn('bank_name');
            $table->dropColumn('bank_account');
            $table->dropColumn('bank_number');
        });
    }
}
