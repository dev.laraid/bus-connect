<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableAllAddSoftDeletes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('payments', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('companies', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('bookings', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('booking_details', function (Blueprint $table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });
        Schema::table('payments', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });
        Schema::table('companies', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });
        Schema::table('booking_details', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });
    }
}
