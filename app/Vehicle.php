<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vehicle extends Model
{
    use SoftDeletes;
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['display_status'];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function bookingDetails()
    {
        return $this->hasMany('App\BookingDetail', 'vehicle_id');
    }

    public function getDisplayStatusAttribute()
    {
        $result = '';
        $status = $this->attributes['status'];

        switch ($status) {
            case '100':
                $result = 'Aktif';
                break;
            case '10':
                $result = 'Tidak Aktif';
                break;
            
            default:
                $result = 'Tidak Ada';
                break;
        }

        return $result;
    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($user) {
            $user->bookingDetails()->delete();
        });
    }
}
