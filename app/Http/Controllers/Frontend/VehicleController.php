<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use Yajra\Datatables\Datatables;
use App\User;
use App\BookingDetail;
use App\Vehicle;
use Auth;
use Session;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $company = Auth::user()->company;

        if ($company->is_verification == 0) {
            return view('frontend.accounts.unverified');
        }
        
        if ($request->ajax()) {

            $vehicles = Vehicle::where('company_id', $company->id);

            if ($request->get('status') != null) {
                if ($request->status != 'all') {
                    $vehicles->whereStatus($request->status);
                }
            }

            $vehicles = $vehicles->select('vehicles.*');

            return Datatables::of($vehicles)
                ->addIndexColumn()
                ->addColumn('action', function ($vehicle) {
                    return view('partials._action', [
                        'model'           => $vehicle,
                        'form_url'        => route('vehicle.destroy', $vehicle->id),
                        'edit_url'        => route('vehicle.edit', $vehicle->id),
                        'show_url'        => route('vehicle.show', $vehicle->id)
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('frontend.vehicles.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('frontend.vehicles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'seat' => 'required|numeric',
            'description' => 'required|string',
            'available' => 'required|numeric',
            'price' => 'required|numeric',
            'image' => 'required|image',
        ]);

        $company_id = Auth::user()->company->id;

        $vehicle = new Vehicle();
        $vehicle->company_id = $company_id;
        $vehicle->name = $request->name;
        $vehicle->seat = $request->seat;
        $vehicle->description = $request->description;
        $vehicle->available = $request->available;
        $vehicle->price = $request->price;

        if ($request->hasFile('image')) {
            // megnambil image yang diupload berikut ekstensinya
            $filename = null;
            $uploaded_image = $request->file('image');
            $extension = $uploaded_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = uniqid() . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'vehicles';
            // memindahkan file ke folder public/images
            $uploaded_image->move($destinationPath, $filename);

            $vehicle->image = $filename;
        }
        $vehicle->save();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Bus dengan nama $vehicle->name berhasil disimpan"
        ]);

        return redirect()->route('vehicle.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vehicle = Vehicle::find($id);
        $company_id = Auth::user()->company->id;

        if (!$vehicle) {
            Session::flash("flash_notification", [
                "level" => "error",
                "message" => "Data Bus Tidak tersedia"
            ]);
            return redirect()->route('vehicle.index');
        }

        if ($vehicle->company_id != $company_id){
            Session::flash("flash_notification", [
                "level" => "error",
                "message" => "Data Bus Tidak tersedia"
            ]);
            return redirect()->route('vehicle.index');
        }

        return view('frontend.vehicles.show')->with(compact('vehicle'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vehicle = Vehicle::find($id);
        $company_id = Auth::user()->company->id;

        if (!$vehicle) {
            Session::flash("flash_notification", [
                "level" => "danger",
                "message" => "Data Bus Tidak tersedia"
            ]);
            return redirect()->route('vehicle.index');
        }

        if ($vehicle->company_id != $company_id) {
            Session::flash("flash_notification", [
                "level" => "danger",
                "message" => "Data Bus Tidak tersedia"
            ]);
            return redirect()->route('vehicle.index');
        }

        return view('frontend.vehicles.edit')->with(compact('vehicle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'seat' => 'required|numeric',
            'description' => 'required|string',
            'available' => 'required|numeric',
            'price' => 'required|numeric',
            'status' => 'required|numeric',
        ]);

        $vehicle = Vehicle::find($id);
        $vehicle->name = $request->name;
        $vehicle->seat = $request->seat;
        $vehicle->description = $request->description;
        $vehicle->available = $request->available;
        $vehicle->price = $request->price;
        $vehicle->status = $request->status;

        if ($request->hasFile('image')) {
            // megnambil image yang diupload berikut ekstensinya
            $filename = null;
            $uploaded_image = $request->file('image');
            $extension = $uploaded_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = uniqid() . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'vehicles';
            // memindahkan file ke folder public/images
            $uploaded_image->move($destinationPath, $filename);

            $vehicle->image = $filename;
        }
        $vehicle->save();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Bus dengan nama $vehicle->name berhasil diubah"
        ]);

        return redirect()->route('vehicle.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vehicle = Vehicle::find($id);

        // check booking
        $check_booking = BookingDetail::where('vehicle_id', $id)->count();
        if ($check_booking > 0) {
            Session::flash("flash_notification", [
                "level" => "warning",
                "message" => "Tidak dapat menghapus bus ini, karena bus ini telah memiliki data penyewaan. Alternatif anda dapat menonaktifkannya"
            ]);
            return redirect()->back();
        }

        /* Disabled for soft deletes 
        if ($vehicle->image) {
            $filepath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'vehicles'
                . DIRECTORY_SEPARATOR . $vehicle->image;
            try {
                File::delete($filepath);
            } catch (FileNotFoundException $e) {
                // File sudah dihapus/tidak ada
            }
        } */

        $vehicle->delete();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Berhasil menghapus data"
        ]);

        return redirect()->route('vehicle.index');
    }
}
