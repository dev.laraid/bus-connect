<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Vehicle;
use App\Booking;
use App\BookingDetail;
use App\Payment;
use Session;

class PageController extends Controller
{
    public function landing(Request $request)
    {
        $vehicles = Vehicle::where('status', 100);

        $condition = (env('DB_CONNECTION', 'mysql') == 'pgsql') ? 'ilike' : 'like';

        if ($request->get('q') != null) {
            if ($request->q != 'all') {
                $vehicles->where('name', $condition, '%' . $request->q . '%');
            }
        }

        $vehicles = $vehicles->paginate(8);

        $q = $request->q;
        $company_id = null;
        if (\Auth::check()) {
            if (\Auth::user()->role == 'member') {
                $company_id = \Auth::user()->company->id;
            }
        }

        return view('frontend.pages.landing')->with(compact('vehicles', 'q', 'company_id'));
    }

    public function pageAbout()
    {
        return view('frontend.pages.about');
    }

    public function showVehicle(Request $request)
    {
        $vehicle = Vehicle::with('company')->find($request->id);

        return response()->json($vehicle);
    }

    public function Cart(Request $request)
    {
        $carts = session('cart') ?? [];
        //dd($carts);
        return view('frontend.pages.cart')->with(compact('carts'));
    }

    public function addToCart(Request $request)
    {
        $id = $request->bus_id;
        $vehicle = Vehicle::find($id);

        $cart = session()->get('cart');
        
        $company_id = $vehicle->company_id;
        $company_name = $vehicle->company->name;
        // if cart is empty then this the first product
        if (!$cart) {
            $cart = [
                $company_id => [
                    'company_name' => $company_name,
                    'data' => [
                        $id => [
                            "id" => $vehicle->id,
                            "name" => $vehicle->name,
                            "quantity" => 1,
                            "available" => $vehicle->available,
                            "image_url" => asset('uploads/images/vehicles/' . $vehicle->image),
                            "seat" => $vehicle->seat,
                            "price" => $vehicle->price
                        ]
                    ]
                ]
            ];

            session()->put('cart', $cart);

            Session::flash("swal_notification", [
                "level" => "success",
                "message" => "Bus berhasil ditambahkan ke list booking"
            ]);

            return redirect()->back();
        }

        // if cart not empty then check if this product exist then increment quantity
        if (isset($cart[$company_id]['data'][$id])) {

            if($cart[$company_id]['data'][$id]['quantity'] < $cart[$company_id]['data'][$id]['available']){
                $cart[$company_id]['data'][$id]['quantity']++;
            }

            session()->put('cart', $cart);

            Session::flash("swal_notification", [
                "level" => "success",
                "message" => "Bus berhasil ditambahkan ke list booking"
            ]);
            return redirect()->back();
        }

        // if item not exist in cart then add to cart with quantity = 1
        $cart[$company_id]['company_name'] = $company_name;
        $cart[$company_id]['data'][$id] = [
            "id" => $vehicle->id,
            "name" => $vehicle->name,
            "quantity" => 1,
            "available" => $vehicle->available,
            "image_url" => asset('uploads/images/vehicles/' . $vehicle->image),
            "seat" => $vehicle->seat,
            "price" => $vehicle->price
        ];

        session()->put('cart', $cart);

        Session::flash("swal_notification", [
            "level" => "success",
            "message" => "Bus berhasil ditambahkan ke list booking"
        ]);
        return redirect()->back();
    }

    public function updateCart(Request $request)
    {
        $status = 0;
        if ($request->company_id and $request->vehicle_id and $request->quantity) {
            $cart = session()->get('cart');

            $cart[$request->company_id]['data'][$request->vehicle_id]['quantity'] = $request->quantity;

            session()->put('cart', $cart);

            $status = 1;
        }
        if ($status == 1) {
            Session::flash("swal_notification", [
                "level" => "success",
                "message" => "Tanggal sewa berhasil ditambahkan"
            ]);
        } else {
            Session::flash("swal_notification", [
                "level" => "error",
                "message" => "Gagal terjadi kesalahan!"
            ]);
        }

        return redirect()->back();
    }

    public function addDateCart(Request $request)
    {
        $status = 0;
        if ($request->company_id and $request->start_date and $request->end_date) {
            $cart = session()->get('cart');

            $cart[$request->company_id]['date_range'] = $request->date_range;
            $cart[$request->company_id]['start_date'] = $request->start_date;
            $cart[$request->company_id]['end_date'] = $request->end_date;
            $cart[$request->company_id]['diff_days'] = $request->diff_days;

            session()->put('cart', $cart);

            $status = 1;
        }
        
        if ($status == 1) {
            Session::flash("swal_notification", [
                "level" => "success",
                "message" => "Tanggal sewa berhasil ditambahkan"
            ]);
        } else {
            Session::flash("swal_notification", [
                "level" => "error",
                "message" => "Gagal terjadi kesalahan!"
            ]);
        }
        return redirect()->back();
    }

    public function removeItemCart(Request $request)
    {
        $status = 0;
        if ($request->company_id and $request->vehicle_id) {

            $cart = session()->get('cart');
            if (isset($cart[$request->company_id]['data'][$request->vehicle_id])) {

                unset($cart[$request->company_id]['data'][$request->vehicle_id]);

                if (empty($cart[$request->company_id]['data'])) {
                    unset($cart[$request->company_id]);
                }

                session()->put('cart', $cart);

                $status = 1;
            }
        }
        if ($status == 1) {
            Session::flash("swal_notification", [
                "level" => "success",
                "message" => "Bus berhasil dihapus"
            ]);
        } else {
            Session::flash("swal_notification", [
                "level" => "error",
                "message" => "Gagal terjadi kesalahan!"
            ]);
        }
        return redirect()->back();
    }

    public function checkout($company_id)
    {
        $cart = session('cart')[$company_id] ?? [];

        if (empty($cart)) {
            abort(404);
        }

        return view('frontend.pages.checkout')->with(compact('cart', 'company_id'));
    }

    public function checkoutStore(Request $request, $company_id)
    {
        $cart = session('cart')[$company_id] ?? [];
        if (empty($cart)) {
            abort(404);
        }

        $booking = new Booking();
        $booking->invoice = strtoupper(uniqid());
        $booking->customer_id = \Auth::user()->id;
        $booking->company_id = $company_id;
        $booking->start_date = $cart['start_date'] ?? date('Y-m-d');
        $booking->end_date = $cart['end_date'] ?? date('Y-m-d');
        $booking->notes = $request->notes;
        $booking->vehicle_total = 0;
        $booking->price_total = 0;
        $booking->payment_method = $request->method;
        $booking->save();

        $price_total = 0;
        $vehicle_total = 0;
        $subtotal = 0;
        foreach ($cart['data'] as $key => $item) {
            $detail = new BookingDetail();
            $detail->booking_id = $booking->id;
            $detail->vehicle_id = $item['id'];
            $detail->quantity = $item['quantity'];
            $detail->price = $item['price'];
            $detail->save();

            $price_total = $item['quantity'] * $item['price'];
            $subtotal += $price_total;
            $vehicle_total += $item['quantity'];
        }

        $booking->vehicle_total = $vehicle_total;
        $booking->price_total = $subtotal * ($cart['diff_days'] ?? 1);
        $booking->save();

        // remove cart
        $carts = session('cart');
        unset($carts[$request->company_id]);
        session()->put('cart', $carts);

        Session::flash("swal_notification", [
            "level" => "success",
            "message" => "Checkout berhasil, silahkan lakukan pembayaran"
        ]);
        return redirect()->route('confirm', $booking->invoice);
    }

    public function confirm($invoice)
    {
        $booking = Booking::whereInvoice($invoice)->where('customer_id', \Auth::user()->id)->first();

        if (!$booking) {
            abort(404);
        }

        $bank = config('app-data.bank')[$booking->payment_method];  
        $banks = json_decode(config('app-data.banks'), true);

        return view('frontend.pages.confirm')->with(compact('booking', 'bank', 'banks'));
    }

    public function confirmStore(Request $request, $invoice)
    {
        $this->validate($request, [
            'confirm_bank' => 'required|string',
            'confirm_account_holder' => 'required|string',
            'payment_image' => 'required|image',
        ]);

        $booking = Booking::whereInvoice($invoice)->first();

        $payment = new Payment();
        $payment->booking_id = $booking->id;
        $payment->method = $request->confirm_bank;
        $payment->account_holder = $request->confirm_account_holder;
        $payment->paid = $booking->price_total;

        if ($request->hasFile('payment_image')) {
            // megnambil image yang diupload berikut ekstensinya
            $filename = null;
            $uploaded_image = $request->file('payment_image');
            $extension = $uploaded_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = uniqid() . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'payments';
            // memindahkan file ke folder public/images
            $uploaded_image->move($destinationPath, $filename);

            // save nama field image
            $payment->image = $filename;
        }
        $payment->save();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Terima kasih, data pembayaran anda berhasil diproses."
        ]);

        return redirect()->route('confirm', $invoice);
    }
}
