<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use Yajra\Datatables\Datatables;
use App\User;
use App\Company;
use App\Booking;
use App\Payout;
use Auth;
use Session;
use Carbon\Carbon;

class PayoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $company_id = Auth::user()->company->id;
            $payouts = Payout::with('company', 'admin')->where('company_id', $company_id);

            if ($request->get('status') != null) {
                if ($request->status != 'all') {
                    $payouts->whereStatus($request->status);
                }
            }

            $payouts = $payouts->select('payouts.*');

            return Datatables::of($payouts)
                ->addIndexColumn()
                ->addColumn('action', function ($payout) {
                    return view('partials._action', [
                        'model'           => $payout,
                        'show_url'        => route('payout.show', $payout->id)
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('frontend.payouts.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $company = Auth::user()->company;

        if ($company->bank_name == null || $company->bank_account == null || $company->bank_number == null) {
            Session::flash("flash_notification", [
                "level" => "warning",
                "message" => "Maaf anda tidak dapat melakukan pencairan, sebelum anda mengisi rincian rekening pada akun anda!"
            ]);
            return redirect()->back();
        }
        
        if (Payout::where('company_id', $company->id)->whereStatus(100)->count() > 0) {
            Session::flash("flash_notification", [
                "level" => "warning",
                "message" => "Maaf anda tidak dapat melakukan pencairan, sebelum pencairan sebelumnya selesai diproses oleh admin!"
            ]);
            return redirect()->back();
        }

        $payouts = Payout::where('company_id', $company->id)->whereIsPayout(1)->sum('nominal');
        $balances = $company->balances;


        $data = [
            'balances' => $balances,
            'payouts' => $payouts
        ];

        return view('frontend.payouts.create')->with(compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nominal' => 'required|numeric',
            'admin_fee' => 'required|string',
            'payout_total' => 'required|string',
        ]);

        $company = Auth::user()->company;

        if ($request->nominal > $company->balances) {
            Session::flash("flash_notification", [
                "level" => "warning",
                "message" => "Nominal pencairan melebihi saldo tersimpan, silahkan cek kembali!"
            ]);
            return redirect()->back();
        }

        $admin_fee = $request->nominal * 0.05;
        $payout_total = $request->nominal - $admin_fee;

        $payout = new Payout;
        $payout->company_id = $company->id;
        $payout->nominal = $request->nominal;
        $payout->admin_fee = $admin_fee;
        $payout->payout_total = $payout_total;
        $payout->save();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Request pencairan saldo berhasil, silahkan menunggu persetujuan admin."
        ]);

        return redirect()->route('payout.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $payout = Payout::find($id);
        $company= Auth::user()->company;

        if (!$payout) {
            Session::flash("flash_notification", [
                "level" => "error",
                "message" => "Data Bus Tidak tersedia"
            ]);
            return redirect()->route('payout.index');
        }

        if ($payout->company_id != $company->id) {
            Session::flash("flash_notification", [
                "level" => "error",
                "message" => "Data Bus Tidak tersedia"
            ]);
            return redirect()->route('payout.index');
        }

        $payouts = Payout::where('company_id', $company->id)->whereIsPayout(1)->sum('nominal');
        $balances = $company->balances;

        $data = [
            'balances' => $balances,
            'payouts' => $payouts
        ];

        return view('frontend.payouts.show')->with(compact('payout', 'data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
