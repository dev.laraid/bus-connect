<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use Yajra\Datatables\Datatables;
use App\User;
use App\Company;
use App\Vehicle;
use App\Booking;
use App\BookingDetail;
use Auth;
use Session;
use Carbon\Carbon;

class TransOutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $customer_id = Auth::user()->id;
            $trans_outs = Booking::with('customer', 'company')->where('customer_id', $customer_id);

            if ($request->get('status') != null) {
                if ($request->status != 'all') {
                    $trans_outs->whereStatus($request->status);
                }
            }

            $trans_outs = $trans_outs->select('bookings.*');

            return Datatables::of($trans_outs)
                ->addIndexColumn()
                ->addColumn('action', function ($trans_out) {
                    return view('partials._action', [
                        'model'           => $trans_out,
                        'confirm_url'     => route('confirm', $trans_out->invoice),
                        'show_url'        => route('transaction_out.show', $trans_out->id)
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('frontend.trans_out.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $booking = Booking::find($id);

        $end = Carbon::parse($booking->end_date);
        $start = Carbon::parse($booking->start_date);
        $diff_days = $end->diffInDays($start) + 1;

        return view('frontend.trans_out.show')->with(compact('booking', 'diff_days'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
