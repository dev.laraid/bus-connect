<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use Yajra\Datatables\Datatables;
use App\User;
use App\Company;
use App\Vehicle;
use App\Booking;
use App\BookingDetail;
use Auth;
use Session;
use Carbon\Carbon;

class TransInController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $company_id = Auth::user()->company->id;
            $trans_ins = Booking::with('customer', 'customer.company')->where('company_id', $company_id);

            if ($request->get('status') != null) {
                if ($request->status != 'all') {
                    $trans_ins->whereStatus($request->status);
                }
            }

            $trans_ins = $trans_ins->select('bookings.*');

            return Datatables::of($trans_ins)
                ->addIndexColumn()
                ->addColumn('action', function ($trans_in) {
                    return view('partials._action', [
                        'model'           => $trans_in,
                        'show_url'        => route('transaction_in.show', $trans_in->id),
                        'reject_url'        => route('transaction_in.edit', $trans_in->id)
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('frontend.trans_in.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $booking = Booking::find($id);

        $end = Carbon::parse($booking->end_date);
        $start = Carbon::parse($booking->start_date);
        $diff_days = $end->diffInDays($start) + 1;

        return view('frontend.trans_in.show')->with(compact('booking', 'diff_days'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $booking = Booking::find($id);

        $end = Carbon::parse($booking->end_date);
        $start = Carbon::parse($booking->start_date);
        $diff_days = $end->diffInDays($start) + 1;

        return view('frontend.trans_in.edit')->with(compact('booking', 'diff_days'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'reject_reason' => 'required|string'
        ]);

        $booking = Booking::find($id);
        $booking->status = 10;
        $booking->reject_reason = $request->reject_reason;
        $booking->save();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Penyewaan ini berhasil dibatalkan"
        ]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
