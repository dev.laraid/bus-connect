<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use Yajra\Datatables\Datatables;
use App\User;
use App\Company;
use Session;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $companies = Company::with('user');

            if ($request->get('verification') != null) {
                if ($request->verification != 'all') {
                    $companies->where('is_verification', $request->verification);
                }
            }

            $companies = $companies->select('companies.*');

            return Datatables::of($companies)
                ->addIndexColumn()
                ->addColumn('action', function ($company) {
                    return view('partials._action', [
                        'model'       => $company,
                        'show_url'    => route('company.show', $company->id)
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('backend.companies.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::find($id);
        $id_crypt = Crypt::encrypt($id);
        return view('backend.companies.show')->with(compact('company', 'id_crypt'));
    }

    public function verification(Request $request)
    {
        $id = Crypt::decrypt($request->tokenizer);

        $company = Company::find($id);
        $company->is_verification = 1;
        $company->save();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Perusahaan ini berhasil diverifikasi"
        ]);

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
