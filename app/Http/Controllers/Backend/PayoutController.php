<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use Yajra\Datatables\Datatables;
use App\User;
use App\Company;
use App\Booking;
use App\Payout;
use Auth;
use Session;
use Carbon\Carbon;

class PayoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $payouts = Payout::with('company', 'admin');

            if ($request->get('status') != null) {
                if ($request->status != 'all') {
                    $payouts->whereStatus($request->status);
                }
            }

            $payouts = $payouts->select('payouts.*');

            return Datatables::of($payouts)
                ->addIndexColumn()
                ->addColumn('action', function ($payout) {
                    return view('partials._action', [
                        'model'           => $payout,
                        'show_url'        => route('payouts.show', $payout->id)
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('backend.payouts.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $payout = Payout::find($id);

        return view('backend.payouts.show')->with(compact('payout'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->status == 10) {
            $this->validate($request, [
                'reject_reason' => 'required|string'
            ]);
            $is_payout = 0;
        } else {
            $this->validate($request, [
                'image' => 'required|image'
            ]);
            $is_payout = 1;
        }

        $payout = Payout::find($id);
        $payout->admin_id = Auth::user()->id;
        $payout->status = $request->status;
        $payout->is_payout = $is_payout;

        if ($request->hasFile('image')) {
            // megnambil image yang diupload berikut ekstensinya
            $filename = null;
            $uploaded_image = $request->file('image');
            $extension = $uploaded_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = uniqid() . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'payouts';
            // memindahkan file ke folder public/images
            $uploaded_image->move($destinationPath, $filename);

            $payout->image = $filename;
        }

        if ($request->status == 10) {
            $payout->reject_reason = $request->reject_reason;
        }

        $payout->save();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Pencairan ini berhasil diproses"
        ]);

        return redirect()->route('payouts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
