<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use Yajra\Datatables\Datatables;
use App\Payment;
use App\Booking;
use App\BookingDetail;
use Auth;
use Session;
use Carbon\Carbon;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $payments = Payment::with('booking');

            if ($request->get('verification') != null) {
                if ($request->verification != 'all') {
                    $payments->whereIsVerification($request->verification);
                }
            }

            $payments = $payments->select('payments.*');

            return Datatables::of($payments)
                ->addIndexColumn()
                ->addColumn('action', function ($payment) {
                    return view('partials._action', [
                        'model'           => $payment,
                        'show_url'        => route('payment.show', $payment->id)
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('backend.payments.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $payment = Payment::find($id);
        $booking = Booking::find($id);

        $end = Carbon::parse($booking->end_date);
        $start = Carbon::parse($booking->start_date);
        $diff_days = $end->diffInDays($start) + 1;

        return view('backend.payments.show')->with(compact('payment', 'booking', 'diff_days'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $payment = Payment::find($id);
        $payment->is_verification = 1;
        $payment->save();

        $booking = Booking::find($payment->booking_id);
        $booking->is_paid = 1;
        $booking->status = 200;
        $booking->save();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Pembayaran ini berhasil diverifikasi"
        ]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
