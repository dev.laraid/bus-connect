<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use Yajra\Datatables\Datatables;
use App\Booking;
use App\BookingDetail;
use Auth;
use Session;
use Carbon\Carbon;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $bookings = Booking::with('customer', 'customer.company');

            if ($request->get('status') != null) {
                if ($request->status != 'all') {
                    $bookings->whereStatus($request->status);
                }
            }

            $bookings = $bookings->select('bookings.*');

            return Datatables::of($bookings)
                ->addIndexColumn()
                ->addColumn('action', function ($booking) {
                    return view('partials._action', [
                        'model'           => $booking,
                        'show_url'        => route('booking.show', $booking->id)
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('backend.bookings.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $booking = Booking::find($id);

        $end = Carbon::parse($booking->end_date);
        $start = Carbon::parse($booking->start_date);
        $diff_days = $end->diffInDays($start) + 1;

        return view('backend.bookings.show')->with(compact('booking', 'diff_days'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $booking = Booking::find($id);
        $booking->status = 300;
        $booking->save();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Penyewaan ini berhasil diselesaikan"
        ]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
