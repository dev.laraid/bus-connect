<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vehicle;
use App\Booking;
use App\Company;
use App\Payout;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $auth = \Auth::user()->role;

        if ($auth == 'admin') {
            return $this->admin();
        } else {
            return $this->member();
        }
    }

    public function admin()
    {
        return view('backend.home');
    }

    public function member()
    {
        /* if ($auth = \Auth::user()->is_verified == 0) {
            return view('frontend.accounts.unverified');
        } */

        $company_id = Auth::user()->company->id;

        $vehicle = Vehicle::where('company_id', $company_id)->count();
        $booking_in = Booking::where('company_id', $company_id)->count();
        $booking_out = Booking::where('customer_id', Auth::user()->id)->count();

        $payouts = Payout::where('company_id', $company_id)->whereIsPayout(1)->sum('nominal');
        
        $this->calculateBalances($company_id, $payouts);

        $balances = Auth::user()->company->balances;

        $data = [
            "vehicle" => $vehicle,
            "booking_in" => $booking_in,
            "booking_out" => $booking_out,
            "balances" => $balances,
            "payouts" => $payouts
        ];

        return view('frontend.home')->with(compact('data'));
    }

    public function calculateBalances($company_id, $payouts)
    {
        $company = Company::find($company_id);
        $income = Booking::where('company_id', $company_id)->whereIsPaid(1)->whereStatus(300)->sum('price_total');

        $company->balances = $income - $payouts;
        $company->save();
    }
}
