<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    use SoftDeletes;

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['display_status'];

    public function details()
    {
        return $this->hasMany('App\BookingDetail');
    }

    public function payment()
    {
        return $this->hasOne('App\Payment', 'booking_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\User', 'customer_id');
    }

    public function company()
    {
        return $this->belongsTo('App\Company', 'company_id');
    }

    public function getDisplayStatusAttribute()
    {
        $result = '';
        $status = $this->attributes['status'];

        switch ($status) {
            case '100':
                $result = 'Menunggu Pembayaran';
                break;
            case '200':
                $result = 'Telah Dibayar';
                break;
            case '300':
                $result = 'Selesai';
                break;
            case '10':
                $result = 'Dibatalkan';
                break;

            default:
                $result = 'Tidak Ada';
                break;
        }

        return $result;
    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($user) {
            $user->payment()->delete();
            $user->details()->delete();
        });
    }
}
