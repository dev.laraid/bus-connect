<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;

    public function user()
    {
        return $this->belongsTo('App\User', 'owner_id');
    }

    public function vehicles()
    {
        return $this->hasMany('App\Vehicle');
    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($user) {
            $user->vehicles()->delete();
        });
    }
}
