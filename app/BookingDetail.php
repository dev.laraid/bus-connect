<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BookingDetail extends Model
{
    use SoftDeletes;
    
    public function booking()
    {
        return $this->belongsTo('App\Booking');
    }

    public function vehicle()
    {
        return $this->belongsTo('App\Vehicle');
    }
}
