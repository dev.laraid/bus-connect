<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Payout extends Model
{
    use SoftDeletes;
    protected $appends = ['display_status', 'display_date'];

    public function admin()
    {
        return $this->belongsTo('App\User', 'admin_id');
    }

    public function company()
    {
        return $this->belongsTo('App\Company', 'company_id');
    }

    public function getDisplayStatusAttribute()
    {
        $result = '';
        $status = $this->attributes['status'];

        switch ($status) {
            case '100':
                $result = 'Menunggu Pencairan';
                break;
            case '200':
                $result = 'Telah Dicairkan';
                break;
            case '10':
                $result = 'Ditolak';
                break;

            default:
                $result = 'Tidak Ada';
                break;
        }

        return $result;
    }

    public function getDisplayDateAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])
            ->format('d M Y');
    }
    
}
